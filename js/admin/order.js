function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}

// ********* To list Restaurants section  ********* //
function order_list(currP) {
		var state		= $("#hid_state").val();
		var type		= $("#hid_type").val();
		var status		= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_order_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/order/order_count",
			data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackBanner,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackBanner(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var state		= $("#hid_state").val();
			var type		= $("#hid_type").val();
			var status		= $("#hid_status").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/order/order_list', 
				data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_order').html('');				 
				$('#load_order').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_order_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var state		= $("#hid_state").val();
	var type		= $("#hid_type").val();
	var status		= $("#hid_status").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/order/order_list/",
	data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	
	success: function (msg) {
		$('#load_order').html('');
		$('#load_order').html(msg);
	}
	});
}

function sort_orders(orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var state		= $("#hid_state").val();
		var type		= $("#hid_type").val();
		var status		= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/order/order_list/",
		data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_order').html('');
				$("#load_order").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/order/order_count",
		data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var state		= $("#hid_state").val();
				var type		= $("#hid_type").val();
				var status		= $("#hid_status").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/order/order_list/', 
				data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
					$('#load_order').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search restaurants
function search_orders()
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var state		= $("#ser_state").val();
		var type		= $("#ser_type").val();
		var status		= $("#ser_status").val();
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_type').val(type);
		$('#hid_state').val(state);
		$('#hid_status').val(status);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/order/order_list/",
		data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_order').html('');
				$("#load_order").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/order/order_count",
		data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var state		= $("#hid_state").val();
				var type		= $("#hid_type").val();
				var status		= $("#hid_status").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/order/order_list/',
				data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_order').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

// Status updation
function change_status_action() 
{
	var current_page = $("[class='current']").html();
	var startp;
	var limitp=10;
	if(current_page==1)
	{
		startp=0;
	}
	else
	{
		startp = (current_page-1)*limitp;
	}
	
	var state		= $("#hid_state").val();
	var type		= $("#hid_type").val();
	var status		= $("#hid_status").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	var orderBy 	= $('#hid_orderBy').val();
	var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
		{
			alert("Please select at least one check box"); 
			return false;
		}
		else{
			$.ajax({
				type: "POST",
				url: baseurl+"admin/order/update_order_status",
				data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&ord_id="+clists+"&action="+action,
				success: function(msg){
					if(msg)
					{ 
						order_list(current_page);	
					}
				}
			   
			});
		}
}

// function to change status of  Properties
function update_order_status(ctrlfnt)	
{
		var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
		var state		= $("#hid_state").val();
		var type		= $("#hid_type").val();
		var status		= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
			
			
		$.ajax({
			type: "POST",
			url: baseurl+ctrlfnt, 
			data:"state="+state+"&type="+type+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp, 
			success: function(msg){
				if(msg)
				{
					order_list(current_page);	
				}
			}
			
		});
}

function view_order_details(ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
		
		$('#manage_head').css('display','none');
		$('#search_order').css('display','none');
		$('#Pagination').css('display','none');
			
		$('#load_order').html('');				 
		$('#load_order').html(msg);	
							
	}				
	});	
}

function back_to_order(currp, order){
	$('#manage_head').css('display','block');
	$('#search_order').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	order_list(currp);
}

function exportForm() {
	if(isDate('date_on'))
    {
		document.exprt.submit();
		$('#date_on').val('');
	}
}