function validate_dishType() {
	$("#dish_type_error").remove();	
	if($("#dish_type").val()=='')
	{
		$("#dish_type").after('<span class="error" id="dish_type_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_DishTypeForm() {
	if(validate_dishType())	{
		document.frmDishType.submit();
	}
}

function validate_DishTypeEditForm(baseurl, currp, order) {
	if(validate_dishType()) {
		var dishType= $("#dish_type").val();
		var typeId	= $("#hidId").val();
	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/dish/update_dish_category", 
			data:"dish_type="+dishType+"&cat_id="+typeId,
			success: function(msg){
				if(msg)
				{ 
					 back_to_dish_category(baseurl, currp, order);
				}
			}
		});
	}
}

// ********* To list dish category section  ********* //
function dish_category_list(baseurl, currP) {
		var category	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_dish_category_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/dish/dish_category_count",
			data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackTypes,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackTypes(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var category	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/dish/dish_category_list', 
				data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_dishcat').html('');				 
				$('#load_dishcat').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_dish_category_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var category	= $("#hid_type").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/dish/dish_category_list",
	data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	
	success: function (msg) {
		$('#load_dishcat').html('');
		$('#load_dishcat').html(msg);
	}
	});
}

function sort_dish_category(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var category	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/dish/dish_category_list/",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_dishcat').html('');
				$("#load_dishcat").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/dish/dish_category_count",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var category	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",

				url: baseurl+'admin/dish/dish_category_list', 
				data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
					$('#load_dishcat').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
} 

//Function To Search 
function search_dish_category(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var category	= $.trim($("#ser_type").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_type').val(category);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/dish/dish_category_list",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_dishcat').html('');
				$("#load_dishcat").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/dish/dish_category_count",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var category	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/dish/dish_category_list',
				data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_dishcat').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}
function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}

function delete_dish_category(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var category	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp, 
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_dishcat').html('');
							$('#load_dishcat').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/dish/dish_category_count",
							data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackNews,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackNews(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var category	= $("#hid_type").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/dish/dish_category_list",
									data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
									success: function(msg){	
									$('#load_dishcat').html('');				 
									$('#load_dishcat').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
}

function edit_dish_category(baseurl, ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
			$('#manage_head').css('display','none');
			$('#search_dishcat').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_dishcat').html('');				 
			$('#load_dishcat').html(msg);	
		}				
	});	
}

function back_to_dish_category(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_dishcat').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	dish_category_list(baseurl,currp);
}
///// ************************** Manage Dish Start here **************************** ////
		
function dishUploadButtonBrowse() {			
	imgpath=baseurl+'js/swfupload/';
	swfu = new SWFUpload({
	// Backend Settings
	upload_url: baseurl+"admin/uploads/upload_dishes",			
	// File Upload Settings
	file_size_limit : "100 MB",	// 2MB
	file_types : "*.jpg;*.gif;*.png;*.jpeg",
	file_types_description : "JPG Images, PNG Images,  GIF Images",
	file_upload_limit : "0",
	
	// Event Handler Settings - these functions as defined in Handlers.js
	//  The handlers are not part of SWFUpload but are part of my website and control how
	//  my website reacts to the SWFUpload events.
	file_queue_error_handler : fileQueueError,
	file_dialog_complete_handler : fileDialogComplete,
	upload_progress_handler : uploadProgress,
	upload_error_handler : uploadError,
	upload_success_handler : uploadSuccessDish,
	upload_complete_handler : uploadComplete,
	// Button Settings
	button_image_url : baseurl+"js/swfupload/images/wdp_buttons_upload_100x102.png",
	button_placeholder_id : "spanButtonPlaceholder",
	button_width: 100,
	button_height: 26,
	button_text : '',
	button_text_style : '',
	button_text_top_padding: 0,
	button_text_left_padding: 18,
	button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
	button_cursor: SWFUpload.CURSOR.HAND,
	// Flash Settings
	flash_url : baseurl+"js/swfupload/swfupload/swfupload.swf",
	
	
	custom_settings : {
		upload_target : "divFileProgressContainer"
	},
	
	// Debug Settings
	debug: false
	});
}

// To remove uploaded banner images
function remove_dish_image(image) {
	if(image) {
		$.ajax({
			type: "POST",
			url: baseurl+"admin/dish/unlink_dish_image_single", 
			data: "image="+image,
			success: function(msg){
				if(msg==1) {
					var aa = $('#thumbnails_dish').find('img[src$="'+baseurl+'uploads/dish/thumb/'+image+'"]');
					var bb = aa.parent();
					bb.remove();
					var hidImg	= $('#dish_img').val();
					var newPimg = hidImg.replace(image, "");
					$('#dish_img').val(newPimg); 
				}
					
			}
		   
		});
	}
		
}

function display_date() {
	if ($('#dish_special').is(':checked')) {
		$("#special_day").show();
		$('#dish_date').blur(function() {
		isDate('dish_date');
		$('#dish_date').blur(validateDishDate);
	});
	} else {
		$("#dish_date_error").remove();	
		$("#special_day").hide();
	} 
	//alert('hi');	
}

function add_rows() {
	var totsize=$('#totsize').val();
	totsize=parseInt(totsize);
	
	var nextid=totsize+1;
	
	var cnt ='<tr id="dish_size_row'+String(nextid)+'"><td width="20%" align="left" valign="top">&nbsp;</td><td width="18%" align="left" valign="top"><div class="formValidation"><input type="text" name="dish_size'+String(nextid)+'" id="dish_size'+String(nextid)+'" class="textbox" style="width:140px;" tabindex="1" /></div></td><td width="4%" align="left" valign="top">Price</td><td width="12%" align="left" valign="top"><div class="formValidation"><input type="text" name="dish_size_price'+String(nextid)+'" id="dish_size_price'+String(nextid)+'" class="textbox" style="width:100px;" tabindex="1" /></div></td><td width="46%" align="left" valign="top"><span class="remove_added"><a onclick="remove_rows('+String(nextid)+');">Remove</a></span></td></tr>';
	
	$('#imgrow').before(cnt);				
  	$('#totsize').val(nextid);
	$('#dish_size_price'+String(nextid)).keyup(function() {
		validateSizePrice(String(nextid));
	});
	$('#dish_size_price'+String(nextid)).blur(function() {
		validateSizePrice(String(nextid));
	});
}

function remove_rows(pos) {
	$('#dish_size_row'+pos).remove();
}

function remove_dish_size_by_id(pos,id) {
	$.ajax({			
		type: "POST",
		url: baseurl+"admin/dish/remove_dish_size_by_id",  
		data: "siz_id=" + id, 
		success: function(msg){	
			$('#dish_size_row'+pos).remove();	
		}				
	});	
}

function validate_DishForm() {
	if ($('#dish_special').is(':checked')) {
		if(validateDishCategory() & validateDishTitle() & validateDishPrice() & validateDishContent() & validateDishImage() & validateDishDate() & !$('.formValidation').find('span').hasClass('error'))
		{		   
			document.frmDish.submit();
		}
	} else {
		if(validateDishCategory() & validateDishTitle() & validateDishPrice() & validateDishContent() & validateDishImage() & !$('.formValidation').find('span').hasClass('error'))
		{		   
			document.frmDish.submit();
		}
	}
}
function validate_DishEditForm(baseurl,currP,order) {
	var dishId	= $("#hidId").val();
	var title	= $("#dish_title").val();
	var cat_id	= $("#dish_category").val();
	var price	= $("#dish_price").val();
	var content	= $('#dish_content').val();
	var dishImg	= $('#dish_img').val();
	/*
	var dishAttr= $("#dish_attr").val();
	var dishAttr='';
	
	if ($('#dish_banner').is(':checked')) {
		var bnrSelct = 1;	
	} else {
		var bnrSelct = 0;
	}
	
	var totsize=$('#totsize').val();
	totsize=parseInt(totsize);
	var total_dishsize='';
	var total_dishprice='';
	var total_dishsizeid='';
	var k =0;
	var dishsize_array	= new Array();
	var dishprice_array	= new Array(); 
	var dishsizeid_array= new Array();
	for(var i=1;i<=totsize;i++) {
		var dish_size	= $('#dish_size'+i).val();
		var dish_price	= $('#dish_size_price'+i).val();
		var dish_size_id= $("#dish_size_hidid"+i).val(); 
		if(dish_size_id)
			dish_size_id = dish_size_id;
		else
			dish_size_id ='';	
		if(dish_size != '') {
			dishsize_array[k] =  dish_size;
			dishprice_array[k] =  dish_price;
			dishsizeid_array[k] =  dish_size_id;
			k=k+1;
		 }
	}
	total_dishsize		=  dishsize_array;
	total_dishprice	 	=  dishprice_array;
	total_dishsizeid 	=  dishsizeid_array;
	*/
	if ($('#dish_special').is(':checked')) {
		if(validateDishCategory() & validateDishTitle() & validateDishPrice() & validateDishContent() & validateDishImage() & validateDishDate() & !$('.formValidation').find('span').hasClass('error'))
		{
			var spcl_item = 1;
			var spcl_date = $('#dish_date').val();
			
			$.ajax({		
				type: "POST",
				url: baseurl+"admin/dish/update_dish_items", 
				//data:"dish_title="+title+"&dish_category="+cat_id+"&dish_id="+dishId+"&dish_price="+price+"&dish_attr="+dishAttr+"&dish_content="+content+"&dish_special="+spcl_item+"&dish_date="+spcl_date+"&dish_img="+dishImg+"&total_dishsize_id="+total_dishsizeid+"&total_dishsize="+total_dishsize+"&total_dishprice="+total_dishprice+"&dish_banner="+bnrSelct,
				data:"dish_title="+title+"&dish_category="+cat_id+"&dish_id="+dishId+"&dish_price="+price+"&dish_content="+content+"&dish_special="+spcl_item+"&dish_date="+spcl_date+"&dish_img="+dishImg,
				success: function(msg){
					if(msg)
					{ 
						 setTimeout('back_to_dish(\''+baseurl+'\','+currP+',\''+order+'\');', 500);
						
					}
				}
			});		   
		}
	} else {
		if(validateDishCategory() & validateDishTitle() & validateDishPrice() & validateDishContent() & validateDishImage() & !$('.formValidation').find('span').hasClass('error'))
		{		   
			var spcl_item = 0;
			var spcl_date ='';
			$.ajax({		
				type: "POST",
				url: baseurl+"admin/dish/update_dish_items", 
				//data:"dish_title="+title+"&dish_category="+cat_id+"&dish_id="+dishId+"&dish_price="+price+"&dish_attr="+dishAttr+"&dish_content="+content+"&dish_special="+spcl_item+"&dish_date="+spcl_date+"&dish_img="+dishImg+"&total_dishsize_id="+total_dishsizeid+"&total_dishsize="+total_dishsize+"&total_dishprice="+total_dishprice+"&dish_banner="+bnrSelct,
				data:"dish_title="+title+"&dish_category="+cat_id+"&dish_id="+dishId+"&dish_price="+price+"&dish_content="+content+"&dish_special="+spcl_item+"&dish_date="+spcl_date+"&dish_img="+dishImg,
				success: function(msg){
					if(msg)
					{ 
						 setTimeout('back_to_dish(\''+baseurl+'\','+currP+',\''+order+'\');', 500);
						
					}
				}
			});	
		}
	}
}


function validateDishCategory()
{
	$("#dish_category_error").remove();	
	if($("#dish_category").val()=='')
	{
		$("#dish_category").after('<span class="error" id="dish_category_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validateDishTitle()
{
	$("#dish_title_error").remove();	
	if($.trim($("#dish_title").val())=='')
	{
		$("#dish_title").after('<span class="error" id="dish_title_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;						
	}
}

function validateDishPrice()
{
	$("#dish_price_error").remove();	
	if($.trim($("#dish_price").val())=='')
	{
		$("#dish_price").after('<span class="error" id="dish_price_error"><span>This field is required</span></span>');
		return false;
	}
	else if(!($("#dish_price").val().match(/^[0-9]+(\.[0-9]+)+$/)) && (IsNumeric($("#dish_price").val())==false) || $("#dish_price").val()==0)  
	{
		$("#dish_price").after('<span class="error" id="dish_price_error" ><span>Enter valid value</span></span>');
		return false;				
	}
	else
	{
		return true;						
	}
}


function validateDishContent()
{
		$("#dish_content_error").remove();	
		if($("#dish_content").val()=='')
		{
			$("#dish_content").after('<span class="error" id="dish_content_error"><span>This field is required</span></span>');
			return false;
		}
		else
		{						
			return true;							
		}
}

function validateDishImage()
{
	$("#dish_image_error").remove();	
	if($.trim($("#dish_img").val())=='')
	{
		$("#thumbnails_dish").html('<span class="error" id="dish_image_error"><span>Image required</span></span>');
		return false;
	}
	else
	{
		return true;						
	}
}

function validateDishDate() {
	var dateValid = isDate('dish_date');
	$("#dish_date_error").remove();	
	if($.trim($("#dish_date").val())=='')
	{
		$("#dish_date").after('<span class="error" id="dish_date_error"><span>This field is required</span></span>');
		return false;
	}
	else if(dateValid==false)
	{
		$("#dish_date").after('<span class="error" id="dish_date_error"><span>Invalid Date</span></span>');
		return false;
	}
	else
	{
		return true;						
	}	
}

function validateSizePrice(pos) 
{
	$("#dish_size_price_error"+pos).remove();
	/*if($("#dish_size_price"+pos).val()=='')
	{
		$("#dish_size_price"+pos).after('<span class="error" id="dish_size_price_error'+pos+'"><span>This field is required</span></span>');
		return false;
	}
	else*/ if(!($("#dish_size_price"+pos).val().match(/^[0-9]+(\.[0-9]+)+$/)) && (IsNumeric($("#dish_size_price"+pos).val())==false) || $("#dish_size_price"+pos).val()==0)  
	{
		$("#dish_size_price"+pos).after('<span class="error" id="dish_size_price_error'+pos+'" ><span>Enter valid value</span></span>');
		return false;				
	}
	else
	{
		return true;
	}
}

function IsNumeric(strString)
{
   var strValidChars = "0123456789";
   var strChar;
   var blnResult = true;
   if (strString.length == 0) return false;

   for (i = 0; i < strString.length && blnResult == true; i++)
	  {
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)
			{
			 blnResult = false;
			}
	  }
   return blnResult;
}

// ********* To list Blog section  ********* //
function dish_list(baseurl, currP) {
		var dish_title	= $("#hid_title").val();
		var dish_status	= $("#hid_status").val();
		var dish_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_dish_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/dish/dish_searchCount",
			data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackUsers,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackUsers(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var dish_title	= $("#hid_title").val();
			var dish_status	= $("#hid_status").val();
			var dish_type	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/dish/dish_list', 
				data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_dish').html('');				 
				$('#load_dish').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_dish_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var dish_title	= $("#hid_title").val();
	var dish_status	= $("#hid_status").val();
	var dish_type	= $("#hid_type").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/dish/dish_list",
	data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function (msg) {
		$('#load_dish').html('');
		$('#load_dish').html(msg);
	}
	});
}

function sort_dish(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var dish_title	= $("#hid_title").val();
		var dish_status	= $("#hid_status").val();
		var dish_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/dish/dish_list/",
		data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_dish').html('');
				$("#load_dish").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/dish/dish_searchCount",
		data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var dish_title	= $("#hid_title").val();
				var dish_status	= $("#hid_status").val();
				var dish_type	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/dish/dish_list',
				data:"dish_titletion="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_dish').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search FAQ
function search_dish(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var dish_title	= $.trim($("#ser_title").val());
		var dish_status	= $.trim($("#ser_status").val());
		var dish_type	= $.trim($("#ser_type").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(dish_title);
		$('#hid_status').val(dish_status);
		$('#hid_type').val(dish_type);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/dish/dish_list",
		data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_dish').html('');
				$("#load_dish").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/dish/dish_searchCount",
		data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var dish_title	= $("#hid_title").val();
				var dish_status	= $("#hid_status").val();
				var dish_type	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/dish/dish_list',
				data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
					$('#load_dish').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}		

// Status updation
function change_status_action(baseurl) 
{
	var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
	
	    var dish_title	= $("#hid_title").val();
		var dish_status	= $("#hid_status").val();
		var dish_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
		{
			alert("Please select at least one check box"); 
			return false;
		}
		else{
			$.ajax({
				type: "POST",
				url: baseurl+"admin/dish/update_dish_status",
				data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&dish_id="+clists+"&action="+action,
				success: function(msg){
					if(msg)
					{ 
						dish_list(baseurl, current_page);	
					}
				}
			   
			});
		}
}

function delete_dish(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var dish_title	= $("#hid_title").val();
			var dish_status	= $("#hid_status").val();
			var dish_type	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_dish').html('');
							$('#load_dish').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/dish/dish_searchCount",
							data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackUsers,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackUsers(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var dish_title	= $("#hid_title").val();
								var dish_status	= $("#hid_status").val();
								var dish_type	= $("#hid_type").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/dish/dish_list",
									data:"dish_title="+dish_title+"&dish_status="+dish_status+"&dish_type="+dish_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
									success: function(msg){	
									$('#load_dish').html('');				 
									$('#load_dish').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  
}

function view_dish(baseurl, ctrlfnt)	
{
	    var current_page = $("[class='current']").html();
		var orderBy 	= $('#hid_orderBy').val();
		$.ajax({			
			type: "POST",
			url: baseurl+ctrlfnt,  
			data: "currP=" + current_page + "&order_by="+orderBy, 
			success: function(msg){	
			
			//$('#manage_siteusers').html('');
			$('#manage_head').css('display','none');
			$('#search_dish').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_dish').html('');				 
			$('#load_dish').html(msg);	
								
		}				
		});	
}

function back_to_dish(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_dish').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	dish_list(baseurl,currp);
}