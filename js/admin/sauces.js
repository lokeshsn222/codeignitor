function IsNumeric(strString)
{
   var strValidChars = "0123456789";
   var strChar;
   var blnResult = true;
   if (strString.length == 0) return false;

   for (i = 0; i < strString.length && blnResult == true; i++)
	  {
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)
			{
			 blnResult = false;
			}
	  }
   return blnResult;
}

function validateSauceCategory() {
	$("#sauce_cat_error").remove();	
	if($("#sauce_cat").val()=='')
	{
		$("#sauce_cat").after('<span class="error" id="sauce_cat_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validateSauceTitle() {
	$("#sauce_title_error").remove();	
	if($("#sauce_title").val()=='')
	{
		$("#sauce_title").after('<span class="error" id="sauce_title_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validateSaucePrice()
{
	$("#sauce_price_error").remove();	
	if($.trim($("#sauce_price").val())!='')
	{
		if(!($("#sauce_price").val().match(/^[0-9]+(\.[0-9]+)+$/)) && (IsNumeric($("#sauce_price").val())==false))  
		{
			$("#sauce_price").after('<span class="error" id="sauce_price_error" ><span>Enter valid value</span></span>');
			return false;				
		}
		else
		{
			return true;						
		}
	}
	else
	{
		return true;						
	}
}

function validateHeatLevel()
{
	$("#heat_level_error").remove();	
	if($.trim($("#heat_level").val())!='')
	{
		if((IsNumeric($("#heat_level").val())==false))  
		{
			$("#heat_level").after('<span class="error" id="heat_level_error" ><span>Enter valid value</span></span>');
			return false;				
		}
		else
		{
			return true;						
		}
	}
	else
	{
		return true;						
	}
}

function validate_SauceForm() {
	if(validateSauceCategory() & validateSauceTitle() & validateSaucePrice() & validateHeatLevel()) {
		document.frmSauces.submit();
	}
}

function validate_SauceEditForm(currp, order) {
	if(validateSauceCategory() & validateSauceTitle() & validateSaucePrice() & validateHeatLevel()) {
		var sauceCat	= $("#sauce_cat").val();
		var sauceTitle	= $("#sauce_title").val();
		var saucePrice	= $("#sauce_price").val();
		var heatLevel	= $("#heat_level").val();
		var sauceId		= $("#hidSauceId").val();
	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/dish/update_sauces", 
			data:"sauce_cat="+sauceCat+"&sauce_title="+sauceTitle+"&sauce_price="+saucePrice+"&heat_level="+heatLevel+"&sauce_id="+sauceId,
			success: function(msg){
				if(msg)
				{ 
					 back_to_sauces(currp, order);
				}
			}
		});
	}
}

function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}

// ********* To list Sauces  ********* //
function sauces_list(currP) {
		var sauces_title= $("#hid_title").val();
		var sauces_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_sauces_list(startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/dish/sauces_searchCount",
			data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackUsers,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackUsers(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var sauces_title= $("#hid_title").val();
			var sauces_type	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/dish/sauces_list', 
				data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_sauces').html('');				 
				$('#load_sauces').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_sauces_list(startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var sauces_title= $("#hid_title").val();
	var sauces_type	= $("#hid_type").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/dish/sauces_list",
	data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function (msg) {
		$('#load_sauces').html('');
		$('#load_sauces').html(msg);
	}
	});
}

function sort_sauces(orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var sauces_title= $("#hid_title").val();
		var sauces_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/dish/sauces_list/",
		data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_sauces').html('');
				$("#load_sauces").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/dish/sauces_searchCount",
		data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var sauces_title= $("#hid_title").val();
				var sauces_type	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/dish/sauces_list',
				data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_sauces').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search FAQ
function search_sauces()
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var sauces_title= $.trim($("#ser_title").val());
		var sauces_type	= $.trim($("#ser_type").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(sauces_title);
		$('#hid_type').val(sauces_type);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/dish/sauces_list",
		data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_sauces').html('');
				$("#load_sauces").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/dish/sauces_searchCount",
		data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var sauces_title= $("#hid_title").val();
				var sauces_type	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/dish/sauces_list',
				data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
					$('#load_sauces').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

function delete_sauces(ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var sauces_title= $("#hid_title").val();
			var sauces_type	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_sauces').html('');
							$('#load_sauces').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/dish/sauces_searchCount",
							data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackUsers,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackUsers(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var sauces_title= $("#hid_title").val();
								var sauces_type	= $("#hid_type").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/dish/sauces_list",
									data:"sauces_title="+sauces_title+"&sauces_type="+sauces_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
									success: function(msg){	
									$('#load_sauces').html('');				 
									$('#load_sauces').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  
}

function edit_sauces(ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
		
		$('#manage_head').css('display','none');
		$('#search_sauces').css('display','none');
		$('#Pagination').css('display','none');
			
		$('#load_sauces').html('');				 
		$('#load_sauces').html(msg);	
							
	}				
	});	
}

function back_to_sauces(currp, order){
	$('#manage_head').css('display','block');
	$('#search_sauces').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	sauces_list(currp);
}