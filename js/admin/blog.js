function validate_blogType() {
	$("#blog_type_error").remove();	
	if($("#blog_type").val()=='')
	{
		$("#blog_type").after('<span class="error" id="blog_type_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_BlogTypeForm() {
	if(validate_blogType())	{
		document.frmBlogType.submit();
	}
}

function validate_BlogTypeEditForm(baseurl, currp, order) {
	if(validate_blogType()) {
		var blogType		= escape($("#blog_type").val());
		var typeId			= $("#hidId").val();
	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/blog/update_blog_category", 
			data:"blog_type="+blogType+"&cat_id="+typeId,
			success: function(msg){
				if(msg)
				{ 
					 back_to_blog_category(baseurl, currp, order);
				}
			}
		});
	}
}

// ********* To list blog category section  ********* //
function blog_category_list(baseurl, currP) {
		var category	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_blog_category_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/blog/blog_category_count",
			data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackTypes,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackTypes(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var category	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/blog/blog_category_list', 
				data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_blogcat').html('');				 
				$('#load_blogcat').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_blog_category_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var category	= $("#hid_type").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/blog/blog_category_list",
	data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	
	success: function (msg) {
		$('#load_blogcat').html('');
		$('#load_blogcat').html(msg);
	}
	});
}

function removeTinyMCE(ID) {
        if ((tinymce==undefined)||(tinymce==null)) {
            return false;
        }
        if (tinymce.getInstanceById(ID))
        {
            tinymce.execCommand('mceFocus', false, ID);                    
            tinymce.execCommand('mceRemoveControl', false, ID);
        }
}

function sort_blog_category(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var category	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/blog/blog_category_list/",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_blogcat').html('');
				$("#load_blogcat").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/blog/blog_category_count",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var category	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",

				url: baseurl+'admin/blog/blog_category_list', 
				data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
					$('#load_blogcat').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
} 

//Function To Search 
function search_blog_category(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var category	= $.trim($("#ser_type").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_type').val(category);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/blog/blog_category_list",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_blogcat').html('');
				$("#load_blogcat").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/blog/blog_category_count",
		data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var category	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/blog/blog_category_list',
				data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_blogcat').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
} 

function delete_blog_category(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var category	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp, 
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_blogcat').html('');
							$('#load_blogcat').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/blog/blog_category_count",
							data:"category="+category+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackNews,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackNews(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var category	= $("#hid_type").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/blog/blog_category_list",
									data:"category="+category+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
									success: function(msg){	
									$('#load_blogcat').html('');				 
									$('#load_blogcat').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
}

function edit_blog_category(baseurl, ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
			$('#manage_head').css('display','none');
			$('#search_blogcat').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_blogcat').html('');				 
			$('#load_blogcat').html(msg);	
		}				
	});	
}

function back_to_blog_category(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_blogcat').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	
	blog_category_list(baseurl,currp);
}



function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}

///////////  ********************************  Blog Post Starts here *************************** ///////
function validate_BlogForm() {
	if(validateBlogCategory() & validateBlogTitle() & validateBlogContent())
	{		   
		document.frmBlog.submit();
	}
}

function validate_BlogEditForm(baseurl,currP,order) {
	
	if(validateBlogCategory() & validateBlogTitle() & validateBlogContent()) {
		var blogId	= $("#hidId").val();
		var title	= escape($("#blog_title").val());
		var cat_id	= $("#blog_category").val();
		content	= escape($.trim(tinyMCE.activeEditor.getContent()));
		//content	= $.trim(tinyMCE.get('blog_content').getContent());	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/blog/update_blog_post", 
			data:"blog_title="+title+"&blog_content="+content+"&blog_id="+blogId+"&blog_category="+cat_id,
			success: function(msg){
				 
			if(msg == 'no') 
			{		
				$("#display").after(' <tr><td valign="top" style="border: #BD0606 1px solid;" id="common_message"  class="error_response_msg" colspan=2 align=left ><span><font color=red>Content cannot be empty</font></span></td></tr>');	
				$("#common_message").slideUp(4000).delay(500);
			}
			if(msg == 'yes') 
			{	
			 removeTinyMCE('blog_content');	
			 setTimeout('back_to_blog(\''+baseurl+'\','+currP+',\''+order+'\');', 500);
			}
					
			}
		});
	}
}

function validateBlogCategory()
{
	$("#blog_category_error").remove();	
	if($("#blog_category").val()=='')
	{
		$("#blog_category").after('<span class="error" id="blog_category_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}
//function for validating News title
function validateBlogTitle()
{
	$("#blog_title_error").remove();	
	if($.trim($("#blog_title").val())=='')
	{
		$("#blog_title").after('<span class="error" id="blog_title_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;						
	}
}

function validateBlogContent()
{
		$("#blog_content_error").remove();	
		content	= $.trim(tinyMCE.activeEditor.getContent());
		//if($.trim(tinyMCE.get('blog_content').getContent())=='')
		if(content=='')
		{
			$("#blog_content").after('<span class="error" id="blog_content_error" style="bottom:25px;"><span>This field is required</span></span>');
			setTimeout('hide_error();', 2500);
			return false;
		}
		else
		{						
			return true;							
		}
}
	
function hide_error() {
	$("#blog_content_error").toggle(1000);
}

// ********* To list Blog section  ********* //
function blog_list(baseurl, currP) {
		var blog_title	= $("#hid_title").val();
		var blog_status	= $("#hid_status").val();
		var blog_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_blog_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/blog/blog_searchCount",
			data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackUsers,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackUsers(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var blog_title	= $("#hid_title").val();
			var blog_status	= $("#hid_status").val();
			var blog_type	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/blog/blog_list', 
				data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_blog').html('');				 
				$('#load_blog').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_blog_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var blog_title	= $("#hid_title").val();
	var blog_status	= $("#hid_status").val();
	var blog_type	= $("#hid_type").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/blog/blog_list",
	data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function (msg) {
		$('#load_blog').html('');
		$('#load_blog').html(msg);
	}
	});
}

function sort_blog(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var blog_title	= $("#hid_title").val();
		var blog_status	= $("#hid_status").val();
		var blog_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/blog/blog_list/",
		data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_blog').html('');
				$("#load_blog").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/blog/blog_searchCount",
		data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var blog_title	= $("#hid_title").val();
				var blog_status	= $("#hid_status").val();
				var blog_type	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/blog/blog_list',
				data:"blog_titletion="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_blog').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search FAQ
function search_blog(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var blog_title	= $.trim($("#ser_title").val());
		var blog_status	= $.trim($("#ser_status").val());
		var blog_type	= $.trim($("#ser_type").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(blog_title);
		$('#hid_status').val(blog_status);
		$('#hid_type').val(blog_type);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/blog/blog_list",
		data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_blog').html('');
				$("#load_blog").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/blog/blog_searchCount",
		data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var blog_title	= $("#hid_title").val();
				var blog_status	= $("#hid_status").val();
				var blog_type	= $("#hid_type").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/blog/blog_list',
				data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
					$('#load_blog').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

// Status updation
function change_status_action(baseurl) 
{
	var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
	
	    var blog_title	= $("#hid_title").val();
		var blog_status	= $("#hid_status").val();
		var blog_type	= $("#hid_type").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
		{
			alert("Please select at least one check box"); 
			return false;
		}
		else{
			$.ajax({
				type: "POST",
				url: baseurl+"admin/blog/update_blog_status",
				data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&blogid="+clists+"&action="+action,
				success: function(msg){
					if(msg)
					{ 
						blog_list(baseurl, current_page);	
					}
				}
			   
			});
		}
}

function delete_blog(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var blog_title	= $("#hid_title").val();
			var blog_status	= $("#hid_status").val();
			var blog_type	= $("#hid_type").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_blog').html('');
							$('#load_blog').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/blog/blog_searchCount",
							data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackUsers,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackUsers(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var blog_title	= $("#hid_title").val();
								var blog_status	= $("#hid_status").val();
								var blog_type	= $("#hid_type").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/blog/blog_list",
									data:"blog_title="+blog_title+"&blog_status="+blog_status+"&blog_type="+blog_type+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
									success: function(msg){	
									$('#load_blog').html('');				 
									$('#load_blog').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  
}

function view_blog(baseurl, ctrlfnt)	
{
	    var current_page = $("[class='current']").html();
		var orderBy 	= $('#hid_orderBy').val();
		$.ajax({			
			type: "POST",
			url: baseurl+ctrlfnt,  
			data: "currP=" + current_page + "&order_by="+orderBy, 
			success: function(msg){	
			
			//$('#manage_siteusers').html('');
			$('#manage_head').css('display','none');
			$('#search_blog').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_blog').html('');				 
			$('#load_blog').html(msg);	
								
		}				
		});	
}

function back_to_blog(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_blog').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	removeTinyMCE('blog_content');
	blog_list(baseurl,currp);
}