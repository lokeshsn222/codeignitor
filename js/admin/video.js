function validate_video_file() {
	$("#image_error").remove();	
	if($("#hide_video_file").val()=='')
	{
		$("#thumbnails").html('<span class="error" id="image_error"><span>Please upload a video </span></span>');
		return false;
	}
	else
	{
		$("#image_error").remove();	
		return true;
	}
}

function validate_video_url()
{
	$("#image_error1").remove();	
	if($.trim($("#youtube_link").val())=='')
	{
		$("#youtube_link").after('<span class="error" id="image_error1" style="bottom:-20px;"><span>Please enter youtube url</span></span>');
		return false;
	}
	else
	{
	   if(/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("#youtube_link").val()))
	   {
		   $("#youtube_link").after('<span class="checked" id="image_error1"><span></span></span>');
		   return true;        
       } 
	  else
	    {
			 $("#image_error1").remove();
		     return false;
		}

    }
  
}

function validate_video_title() {
	$("#videotitle_error").remove();	
	if($("#videotitle").val()=='')
	{
		$("#videotitle").after('<span class="error" id="videotitle_error"><span>Please Enter Video Title </span></span>');
		return false;
	}
	else
	{
		$("#videotitle_error").remove();
		return true;
	}
}

function hideResponseMsg()
{
	$("#msg").slideToggle('slow');
	window.location.href="video_gallery";
	
}

// ********* To list Video Gallery section  ********* //
function video_list(baseurl, currP) {
		var vg_title	= $("#hid_title").val();
		var vg_status	= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_video_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/gallery/video_count",
			data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackVideos,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackVideos(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var vg_title	= $("#hid_title").val();
			
			var vg_status	= $("#hid_status").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/gallery/video_list', 
				data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
				$('#load_videos').html('');				 
				$('#load_videos').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_video_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var vg_title	= $("#hid_title").val();
	var vg_status	= $("#hid_status").val();
	
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/gallery/video_list",
	data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	success: function (msg) {
		$('#load_videos').html('');
		$('#load_videos').html(msg);
	}
	});
}

function sort_videos(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var vg_title	= $("#hid_title").val();
		var vg_status	= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/gallery/video_list",
		data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_videos').html('');
				$("#load_videos").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/gallery/video_count",
		data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var vg_title	= $("#hid_title").val();
				var vg_status	= $("#hid_status").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+"admin/gallery/video_list",
				data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
					$('#load_videos').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search Video Gallery
function search_videos(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var vgTitle		= $.trim($("#ser_title").val());
		var vgStatus	= $.trim($("#ser_status").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(vgTitle);
		$('#hid_status').val(vgStatus);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/gallery/video_list",
		data:"vg_title="+vgTitle+"&vg_status="+vgStatus+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_videos').html('');
				$("#load_videos").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/gallery/video_count",
		data:"vg_title="+vgTitle+"&vg_status="+vgStatus+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var vg_title	= $("#hid_title").val();
				var vg_status	= $("#hid_status").val();
				
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+"admin/gallery/video_list",
				data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
					$('#load_videos').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

// Status updation
function change_status_action(baseurl) 
{
	var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
	
	    var vg_title	= $("#hid_title").val();
		var vg_status	= $("#hid_status").val();
		
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
			{
				alert("Please select at least one check box"); 
				return false;
			}
			else{
				$.ajax({
					type: "POST",
					url: baseurl+"admin/gallery/update_video_status",
					data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&vg_id="+clists+"&action="+action,
					success: function(msg){
						if(msg)
						{ 
							video_list(baseurl, current_page);	
						}
					}
				   
				});
			}
}

function delete_video(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var vg_title	= $("#hid_title").val();
			var vg_status	= $("#hid_status").val();
			
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_videos').html('');
							$('#load_videos').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/gallery/video_count",
							data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackNews,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackNews(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var vg_title	= $("#hid_title").val();
								var vg_status	= $("#hid_status").val();
								
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/gallery/video_list",
									data:"vg_title="+vg_title+"&vg_status="+vg_status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
									success: function(msg){	
									$('#load_videos').html('');				 
									$('#load_videos').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
}

function view_video(baseurl, ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
		
		$('#manage_head').css('display','none');
		$('#search_videos').css('display','none');
		$('#Pagination').css('display','none');
			
		$('#load_videos').html('');				 
		$('#load_videos').html(msg);	
							
	}				
	});	
}

function edit_video(baseurl, ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
			$('#manage_head').css('display','none');
			$('#search_videos').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_videos').html('');				 
			$('#load_videos').html(msg);	
		}				
	});	
}

function back_to_videos(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_videos').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	video_list(baseurl,currp);
}

function validate_videoEditForm(baseurl, currp, order) {	   
	if( validate_video_title() & validate_video_file())
	{
		var hide_video_file	= $("#hide_video_file").val();
		var videoTitle		= $("#videotitle").val();
		var videoDesc		= $("#videodesc").val();
		var vId				= $("#hidId").val();
				
		var $url= baseurl+"admin/gallery/update_video";
		$.ajax(
		{
			type:'POST', 
			url:$url,
			data:"hide_video_file="+hide_video_file+"&video_title="+videoTitle+"&video_desc="+videoDesc+"&v_id="+vId,
			success: function(message)
			{
				back_to_videos(baseurl, currp, order);
				
			}
		});
	}
	
}

function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}