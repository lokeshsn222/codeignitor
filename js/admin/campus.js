function loadState(cId) {
	$.ajax({		
		type: "POST",
		url: baseurl+"admin/Cms/get_campus_state/"+cId, 
		data:"",
		success: function(msg){
			$("#state_load").html(msg);
		}
	});
}

function add_rows() {
	var totbuilding=$('#totbuilding').val();
	totbuilding=parseInt(totbuilding);
	
	var nextid=totbuilding+1;
	var cnt ='<tr id="building_row'+String(nextid)+'"><td>&nbsp;</td><td><div class="formValidation"><input type="text" name="building'+String(nextid)+'" id="building'+String(nextid)+'" class="textbox" /></div></td><td align="left" valign="top"><span class="remove_added"><a onclick="remove_rows('+String(nextid)+');">Remove</a></span></td></tr>';
	
	$('#after_building_row').before(cnt);				
  	$('#totbuilding').val(nextid);
}

function remove_rows(pos) {
	$('#building_row'+pos).remove();
}

function remove_rows_by_id(pos,id) {
	$.ajax({			
		type: "POST",
		url: baseurl+"admin/Cms/remove_campus_dt_by_id",  
		data: "bid=" + id, 
		success: function(msg){	
			$('#building_row'+pos).remove();	
		}				
	});	
}

function validate_CampusForm() {
	if(validateCountry() & validateState() & validateCampus() & validateBuilding())
	{		   
		document.frmCmps.submit();
	}
}

function validate_CampusUploadForm() {
	if(validateCountry() & validateState() & validateCampusFile())
	{		   
		document.frmCmps.submit();
	}
}

function validate_CampusDtUploadForm() {
	if(validateCampus() & validateCampusDtFile())
	{		   
		document.frmCmpsdt.submit();
	}
}

function validate_CampusEditForm(baseurl,currP,order) {
	if(validateCountry() & validateState() & validateCampus() & validateBuilding()) {
		var campusId	= $("#hidId").val();
		var country		= $("#country").val();
		var state		= $("#state").val();
		var campus		= escape($("#campus").val());
		
		var totbuilding=$('#totbuilding').val();
		totbuilding=parseInt(totbuilding);
		var total_building='';
		var k =0;
		var building_array=new Array();
		for(var i=1;i<=totbuilding;i++) {
			var building=$('#building'+i).val();
			if(building != '') {
				building_array[k] =  building;
				k=k+1;
			 }
		}
		total_building =  building_array;
		
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/Cms/update_campus", 
			data:"country="+country+"&state="+state+"&campus="+campus+"&campus_id="+campusId+"&total_building="+total_building,
			success: function(msg){
				if(msg)
				{ 
					 setTimeout('back_to_campus(\''+baseurl+'\','+currP+',\''+order+'\');', 500);
					
				}
			}
		});		   
	}
}

function validateCountry() {
	$("#country_error").remove();	
	if($("#country").val()=='')
	{
		$("#country").after('<span class="error" id="country_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}	
}

function validateState() {
	$("#state_error").remove();	
	if($("#state").val()=='')
	{
		$("#state").after('<span class="error" id="state_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}	
}

function validateCampus() {
	$("#campus_error").remove();	
	if($("#campus").val()=='')
	{
		$("#campus").after('<span class="error" id="campus_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}	
}

function validateBuilding() {
	$("#building1_error").remove();	
	if($("#building1").val()=='')
	{
		$("#building1").after('<span class="error" id="building1_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}	
}

function validateCampusFile() {
	$("#campus_error").remove();	
	if($("#campus").val()=='')
	{
		$("#campus").after('<span class="error" id="campus_error"><span>This field is required</span></span>');
		setTimeout(function(){$("#campus_error").remove()}, 2500);	
		return false;
	}
	else
	{
		return true;
	}		
}

function validateCampusDtFile() {
	$("#buildings_error").remove();	
	if($("#buildings").val()=='')
	{
		$("#buildings").after('<span class="error" id="buildings_error"><span>This field is required</span></span>');
		setTimeout(function(){$("#buildings_error").remove()}, 2500);	
		return false;
	}
	else
	{
		return true;
	}
}

function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}

// ********* To list Campus section  ********* //
function campus_list(baseurl, currP) {
		var title		= $("#hid_title").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_campus_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/Cms/campus_count",
			data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackBanner,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackBanner(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var title		= $("#hid_title").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/Cms/campus_list', 
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_campus').html('');				 
				$('#load_campus').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_campus_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var title		= $("#hid_title").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/Cms/campus_list/",
	data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	
	success: function (msg) {
		$('#load_campus').html('');
		$('#load_campus').html(msg);
	}
	});
}

function sort_campus(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var title		= $("#hid_title").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/Cms/campus_list/",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_campus').html('');
				$("#load_campus").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/Cms/campus_count",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var title		= $("#hid_title").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/Cms/campus_list', 
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
					$('#load_campus').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search campus
function search_campus(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var title		= $.trim($("#ser_title").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(title);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/Cms/campus_list",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_campus').html('');
				$("#load_campus").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/Cms/campus_count",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var title		= $("#hid_title").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/Cms/campus_list',
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_campus').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

// Status updation
function change_status_action(baseurl) 
{
	var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
	
	    var title		= $("#hid_title").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
		{
			alert("Please select at least one check box"); 
			return false;
		}
		else{
			$.ajax({
				type: "POST",
				url: baseurl+"admin/Cms/update_campus_status",
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&campus_id="+clists+"&action="+action,
				success: function(msg){
					if(msg)
					{ 
						campus_list(baseurl, current_page);	
					}
				}
			   
			});
		}
}

function delete_campus(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var title		= $("#hid_title").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp, 
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_campus').html('');
							$('#load_campus').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/Cms/campus_count",
							data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackBanner,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackBanner(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var title		= $("#hid_title").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/Cms/campus_list",
									data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
									success: function(msg){	
									$('#load_campus').html('');				 
									$('#load_campus').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  
}

function view_campus(baseurl, ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
		
		$('#manage_head').css('display','none');
		$('#search_campus').css('display','none');
		$('#Pagination').css('display','none');
			
		$('#load_campus').html('');				 
		$('#load_campus').html(msg);	
							
	}				
	});	
}

function back_to_campus(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_campus').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	campus_list(baseurl,currp);
}