// To remove uploaded gallery images
function remove_gallery_image(image) {
	if(image) {
		$.ajax({
			type: "POST",
			url: baseurl+"admin/gallery/unlink_gallery_image_single", 
			data: "image="+image,
			success: function(msg){
				if(msg==1) {
					var aa = $('#thumbnails_gallery').find('img[src$="'+baseurl+'uploads/gallery/thumb/'+image+'"]');
					var bb = aa.parent();
					bb.remove();
					var hidImg	= $('#gallery_img').val();
					var newPimg = hidImg.replace(image, "");
					$('#gallery_img').val(newPimg); 
				}
					
			}
		   
		});
	}
		
}

// manage Gallery starts
function validate_PhotoForm() {
	// & validateGalleryDescription()
   if(validateGalleryTitle()  & validateGalleryImage() )
   {		   
	   document.frmPhoto.submit();
   }
}

function validate_PhotoEditForm(baseurl, currp, order) {
	//& validateGalleryDescription()
   if(validateGalleryTitle()  & validateGalleryImage())
   {	
   		var galTitle	= $("#gallery_title").val();
		var galContent	= $("#gallery_description").val();
		var galImg		= $("#gallery_img").val();
		var galId		= $("#hidId").val();
	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/gallery/update_photo_gallery", 
			data:"gallery_title="+galTitle+"&gallery_description="+galContent+"&gallery_img="+galImg+"&gal_id="+galId,
			success: function(msg){
				if(msg)
				{
					$('#gallery_list').html('');				 
					$('#gallery_list').html(msg);
					//upload_more_photo_gallery(baseurl, currp, order, galId); 
					 back_to_photo_gallery(baseurl, currp, order);
				}
			}
		});
   }
}

function upload_more_photo_gallery(baseurl, currp, order, galId) {
	$.ajax({		
			type: "POST",
			url: baseurl+"admin/gallery/upload_more_photo_gallery", 
			data:"gal_id="+galId+"&currp="+currp+"&order="+order+"&more_add=1",
			success: function(msg){
				if(msg)
				{
					$('#gallery_list').html('');				 
					$('#gallery_list').html(msg);
					// back_to_photo_gallery(baseurl, currp, order);
				}
			}
		});
}
	
function updateFrontImg(val) {
	var galId = $('#hidGalid').val();
	if(val!='' && galId!='') {
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/gallery/update_front_image", 
			data:"img_id="+val+"&gal_id="+galId,
			success: function(msg){
			}
		});	
	}
}	 
//function for validating title
function validateGalleryTitle()
{
	$("#gallery_title_error").remove();	
	if($.trim($("#gallery_title").val())=='')
	{
		$("#gallery_title").after('<span class="error" id="gallery_title_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		$("#gallery_title_error").remove();
		return true;						
	}
}
//function for validating Description
function validateGalleryDescription()
{
	$("#gallery_description_error").remove();	
	if($.trim($("#gallery_description").val())=='')
	{
	$("#gallery_description").after('<span class="error" id="gallery_description_error" ><span>This field is required</span></span>');
	return false;
	}
	else
	{						
		$("#gallery_description_error").remove();
		return true;							
	}
}

function validateGalleryImage() {
	$("#gallery_img_error").remove();	
	if($("#gallery_img").val()=='')
	{
	$("#gallery_img").after('<span class="error" id="gallery_img_error" style="position:static;"><span>Image required</span></span>');
	return false;
	}
	else
	{
		return true;
	}
}

function PhotoGallerylist(baseurl, currP) {
	    $('#search_gallery').show();
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP!='') {
			var startp;
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_gallery_list(baseurl,startp,limitp);						  		
        var start_on = $('#hid_start_on').val();
	    var end_on = $('#hid_end_on').val();
	    var keyword = $('#hid_keyword').val();
		$.ajax({
			type:"POST",
			url:baseurl+"admin/gallery/photo_gallery_count",
			data:"start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 10,
					callback: pageselectCallbackPhoto,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackPhoto(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy = $('#hid_orderBy').val();
			var start_on = $('#hid_start_on').val();
	        var end_on = $('#hid_end_on').val();
	        var keyword = $('#hid_keyword').val();
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/gallery/load_photo_gallery', 
				data:"startp="+page_ind+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
				success: function(msg){	
				$('#gallery_list').html('');				 
				$('#gallery_list').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}



function load_gallery_list(baseurl,startp,limitp) {
	var orderBy 	= $('#hid_orderBy').val();
	var start_on = $('#hid_start_on').val();
	var end_on = $('#hid_end_on').val();
	var keyword = $('#hid_keyword').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/gallery/load_photo_gallery",
	data:"startp="+startp+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
	success: function (msg) {
		$('#gallery_list').html('');
		$('#gallery_list').html(msg);
	}
	});
}

//Function To Sort Gallery
function sort_photo_gallery(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}
	//alert(orderBy);
	
		var startp=0;
	    var limitp=10;
		$('#hid_orderBy').val(orderBy);
		var start_on = $('#hid_start_on').val();
	    var end_on = $('#hid_end_on').val();
	    var keyword = $('#hid_keyword').val();
		$.ajax({
		type: "POST",
		url: baseurl + "admin/gallery/load_photo_gallery",
		data: "startp="+startp+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword, 
		success: function(msg){
			if(msg)
			{
				$('#gallery_list').html('');
				$("#gallery_list").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/gallery/photo_gallery_count",
		data:"start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
		success:function(msgCount)
		{
			//alert(msgCount);
			
			if(msgCount!=0 && msgCount > limitp) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msgCount, {
				num_edge_entries: 2,
				num_display_entries: 10,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	 
			}
			if(msgCount==0) {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				var orderBy 	= $('#hid_orderBy').val();
				var start_on = $('#hid_start_on').val();
	            var end_on = $('#hid_end_on').val();
	            var keyword = $('#hid_keyword').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/gallery/load_photo_gallery', 
				data:"startp="+page_ind+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
				success: function(msg){	
					$('#gallery_list').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

function search_photo_gallery(baseurl) {
	if(isDate('start_on') && isDate('end_on'))
    {
		var hidCurrP = $('#hid_currP').val();
		var start_on = $('#start_on').val();
		var end_on = $('#end_on').val();
		var keyword = $('#keyword').val();
		$('#hid_keyword').val(keyword);
		$('#hid_start_on').val(start_on);
		$('#hid_end_on').val(end_on);
		
		
		var limitp=10;
		if(hidCurrP!='') {
			var startp;
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_gallery_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/gallery/photo_gallery_count",
			data:"start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
			success:function(msg)
			{   
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 10,
					callback: pageselectCallbackSearch,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackSearch(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			var start_on = $('#hid_start_on').val();
	        var end_on = $('#hid_end_on').val();
	        var keyword = $('#hid_keyword').val();
			var orderBy = $('#hid_orderBy').val();
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/gallery/load_photo_gallery', 
				data:"startp="+page_ind+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
				success: function(msg){	
				$('#gallery_list').html('');				 
				$('#gallery_list').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
	}
}

// Status updation
function change_status_action(baseurl) 
{
	var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
	
	    var start_on	= $('#hid_start_on').val();
		var end_on 		= $('#hid_end_on').val();
		var keyword 	= $('#hid_keyword').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
			{
				alert("Please select at least one check box"); 
				return false;
			}
			else{
				$.ajax({
					type: "POST",
					url: baseurl+"admin/gallery/update_gallery_status",
					data:"keyword="+keyword+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&g_id="+clists+"&action="+action,
					success: function(msg){
						if(msg)
						{ 
							PhotoGallerylist(baseurl, current_page);	
						}
					}
				   
				});
			}
}
// function to delete Gallery
function delete_photo_gallery(baseurl, ctrlfnt)	
{
	
	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
		var orderBy 	= $('#hid_orderBy').val();
		var start_on = $('#hid_start_on').val();
	    var end_on = $('#hid_end_on').val();
	    var keyword = $('#hid_keyword').val();	
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,  
					data: "startp="+startp+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword, 
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#gallery_list').html('');
							$('#gallery_list').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/gallery/photo_gallery_count",
							data:"start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 10,
									callback: pageselectCallbackRmv,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
									
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackRmv(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								var orderBy 	= $('#hid_orderBy').val();
								var start_on = $('#hid_start_on').val();
	                            var end_on = $('#hid_end_on').val();
	                            var keyword = $('#hid_keyword').val();						
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/gallery/load_photo_gallery",
									data: "startp="+page_ind+"&limitp="+limitp+"&order_by="+orderBy+"&start_on="+start_on+"&end_on="+end_on+"&keyword="+keyword, 
									success: function(msg){	
									$('#gallery_list').html('');				 
									$('#gallery_list').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  }
  
 function edit_photo_gallery(baseurl, ctrlfnt)	
 {
	$('#search_gallery').hide();
	var current_page = $("[class='current']").html();
	if(current_page) {
	current_page = current_page;	
	} else {
		current_page =1;	
	}
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
			type: "POST",
			url: baseurl+ctrlfnt,  
			data: "currP=" + current_page + "&order_by="+orderBy, 
			success: function(msg){	
			$('#manage_head').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#gallery_list').html('');				 
			$('#gallery_list').html(msg);	
								
		}				
		});	
		
}

function view_photo_gallery(baseurl, ctrlfnt) {
	$('#search_gallery').hide();
	var current_page = $("[class='current']").html();
	if(current_page) {
	current_page = current_page;	
	} else {
		current_page =1;	
	}
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
			type: "POST",
			url: baseurl+ctrlfnt,  
			data: "currP=" + current_page + "&order_by="+orderBy, 
			success: function(msg){	
			$('#manage_head').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#gallery_list').html('');				 
			$('#gallery_list').html(msg);	
								
		}				
		});	
}


function back_to_photo_gallery(baseurl, currp, order){
	
	$('#manage_head').css('display','block');
	$('#Pagination').css('display','block');
	
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	PhotoGallerylist(baseurl);
	
}

function add_more_photos(baseurl, galID)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$('#manage_head').css('display','none');
	$('#search_gallery').css('display','none');
	$('#Pagination').css('display','none');
	$('#gallery_list').html('');
	upload_more_photo_gallery(baseurl, current_page, orderBy, galID);							
}

function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}