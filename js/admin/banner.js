// To remove uploaded banner images
function remove_banner_image(image) {
	if(image) {
		$.ajax({
			type: "POST",
			url: baseurl+"admin/banner/unlink_banner_image_single", 
			data: "image="+image,
			success: function(msg){
				if(msg==1) {
					var aa = $('#thumbnails_banner').find('img[src$="'+baseurl+'uploads/banner/thumb/'+image+'"]');
					var bb = aa.parent();
					bb.remove();
					var hidImg	= $('#banner_img').val();
					var newPimg = hidImg.replace(image, "");
					$('#banner_img').val(newPimg); 
				}
					
			}
		   
		});
	}
		
}

function getSubTitles(type, id) {
	if(id)
		id = id;
	else 
		id ='';	
	 $.ajax({
		type: "POST",
		url: baseurl+"admin/banner/get_banner_titles/"+type+"/"+id,  
		data: "", 
		success: function(msg){
			document.getElementById('item_title_load').innerHTML=msg;
			$('#bnr_item_id').change(validate_bnrSubItem);
		}
	});
}

function validate_bnrForm() {
	if(validate_bnrTitle() & validate_bnrItem() & validate_bnrSubItem() & validate_bnrImage()) {
		document.frmBnr.submit();	
	}
}

function validate_bnrEditForm(baseurl, currp, order) {
	if(validate_bnrTitle() & validate_bnrItem() & validate_bnrSubItem() & validate_bnrImage()) {
		var bnTitle		= $("#bnr_title").val();
		var bnItem		= $("#bnr_item").val();
		var bnItemId            = $("#bnr_item_id").val();
		var bnImag		= $("#banner_img").val();
                var bnDesc		= $("#bnr_desc").val();
		var bId			= $("#hidId").val();
	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/banner/update_banner", 
			data:"bn_title="+bnTitle+"&bn_item="+bnItem+"&bn_itemid="+bnItemId+"&bn_image="+bnImag+"&bn_id="+bId+"&bn_desc="+bnDesc,
			success: function(msg){
				if(msg)
				{ 
					 back_to_banner(baseurl, currp, order);
				}
			}
		});
	}
}

function validate_bnrTitle()
{
	$("#bn_title_error").remove();	
	if($("#bnr_title").val()=='')
	{
	$("#bnr_title").after('<span class="error" id="bn_title_error"><span>This field is required</span></span>');
	return false;
	}
	else
	{
		return true;
	}
}

function validate_bnrItem()
{
	$("#bnr_item_error").remove();	
	if($("#bnr_item").val()=='')
	{
	$("#bnr_item").after('<span class="error" id="bnr_item_error"><span>This field is required</span></span>');
	return false;
	}
	else
	{
		return true;
	}
}

function validate_bnrSubItem()
{
	$("#bnr_item_id_error").remove();	
	if($("#bnr_item_id").val()=='')
	{
	$("#bnr_item_id").after('<span class="error" id="bnr_item_id_error"><span>This field is required</span></span>');
	return false;
	}
	else
	{
		return true;
	}
}

function validate_bnrImage()
{
	$("#banner_img_error").remove();	
	if($("#banner_img").val()=='')
	{
	$("#banner_img").after('<span class="error" id="banner_img_error" style="position:static;"><span>Image required</span></span>');
	return false;
	}
	else
	{
		return true;
	}
}

// ********* To list banner section  ********* //
function banner_list(baseurl, currP) {
		var title		= $("#hid_title").val();
		var status		= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_banner_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/banner/banner_searchCount",
			data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackBanner,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackBanner(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var title		= $("#hid_title").val();
			var status		= $("#hid_status").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/banner/banner_list', 
				data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_banner').html('');				 
				$('#load_banner').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_banner_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var title		= $("#hid_title").val();
	var status		= $("#hid_status").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/banner/banner_list/",
	data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	
	success: function (msg) {
		$('#load_banner').html('');
		$('#load_banner').html(msg);
	}
	});
}

function sort_banner(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var title		= $("#hid_title").val();
		var status		= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/banner/banner_list/",
		data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_banner').html('');
				$("#load_banner").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/banner/banner_searchCount",
		data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var title		= $("#hid_title").val();
				var status		= $("#hid_status").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/banner/banner_list', 
				data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
					$('#load_banner').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search banner image
function search_banner(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var title		= $.trim($("#bn_title").val());
		var status		= $.trim($("#bn_status").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(title);
		$('#hid_status').val(status);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/banner/banner_list",
		data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_banner').html('');
				$("#load_banner").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/banner/banner_searchCount",
		data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var title		= $("#hid_title").val();
				var status		= $("#hid_status").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/banner/banner_list',
				data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_banner').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
				

}

// Status updation
function change_status_action(baseurl) 
{
	var current_page = $("[class='current']").html();
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
	
	    var title		= $("#hid_title").val();
		var status		= $("#hid_status").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		var action		= $('#action').val();
	
	
	
	var clists = $('input[name="ListStatusLinkCheckbox[]"]:checked').map(function(){return this.value;}).get();
		if(clists=='')
			{
				alert("Please select at least one check box"); 
				return false;
			}
			else{
				$.ajax({
					type: "POST",
					url: baseurl+"admin/banner/update_banner_status",
					data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp+"&bid="+clists+"&action="+action,  
					
					success: function(msg){
						if(msg)
						{ 
							banner_list(baseurl, current_page);	
						}
					}
				   
				});
			}
}

function delete_banner(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var title		= $("#hid_title").val();
			var status		= $("#hid_status").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp, 
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_banner').html('');
							$('#load_banner').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/banner/banner_searchCount",
							data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackBanner,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackBanner(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var title		= $("#hid_title").val();
								var status		= $("#hid_status").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/banner/banner_list",
									data:"title="+title+"&status="+status+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
									success: function(msg){	
									$('#load_banner').html('');				 
									$('#load_banner').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  
}

function edit_banner(baseurl, ctrlfnt)	
{
	    var current_page = $("[class='current']").html();
		var orderBy 	= $('#hid_orderBy').val();
		$.ajax({			
			type: "POST",
			url: baseurl+ctrlfnt,  
			data: "currP=" + current_page + "&order_by="+orderBy, 
			success: function(msg){	
			
			//$('#manage_siteusers').html('');
			$('#manage_head').css('display','none');
			$('#search_banner').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_banner').html('');				 
			$('#load_banner').html(msg);	
								
		}				
		});	
}

function view_banner(baseurl, ctrlfnt)	
{
	    var current_page = $("[class='current']").html();
		var orderBy 	= $('#hid_orderBy').val();
		$.ajax({			
			type: "POST",
			url: baseurl+ctrlfnt,  
			data: "currP=" + current_page + "&order_by="+orderBy, 
			success: function(msg){	
			
			$('#manage_head').css('display','none');
			$('#search_banner').css('display','none');
			$('#Pagination').css('display','none');
				
			$('#load_banner').html('');				 
			$('#load_banner').html(msg);	
								
		}				
		});	
}

function back_to_banner(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_banner').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	banner_list(baseurl,currp);
}


function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}