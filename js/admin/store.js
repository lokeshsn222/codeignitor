function validate_StoreForm() {
	if(validate_rest_nameField() & validate_rest_phoneField() & validate_rest_addressField() & validate_rest_mapAddressField() & validate_rest_deliveryField() & validate_rest_carryoutField()) {
		document.frmStr.submit();	
	}
}

function validate_StoreEditForm(baseurl, currp, order) {
	if(validate_rest_nameField() & validate_rest_phoneField() & validate_rest_addressField() & validate_rest_mapAddressField() & validate_rest_deliveryField() & validate_rest_carryoutField()) {
		var resTitle	= $("#rest_name").val();
		var resPhone	= $("#rest_phone").val();
		var resAddress	= escape($("#rest_address").val());
		var resMapAddress= escape($("#rest_map_address").val());
		var resDelivery	= $("#rest_delivery").val();
		var resCarryout	= $("#rest_carryout").val();
		var resId		= $("#hidId").val();
	
		$.ajax({		
			type: "POST",
			url: baseurl+"admin/store/update_restaurants", 
			data:"rest_name="+resTitle+"&rest_phone="+resPhone+"&rest_address="+resAddress+"&rest_delivery="+resDelivery+"&rest_carryout="+resCarryout+"&rs_id="+resId+"&rest_map_address="+resMapAddress,
			success: function(msg){
				if(msg)
				{ 
					 back_to_restaurants(baseurl, currp, order);
				}
			}
		});
	}
}


function validate_rest_nameField() {
	$("#rest_name_error").remove();	
	if($("#rest_name").val()=='')
	{
		$("#rest_name").after('<span class="error" id="rest_name_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_rest_phoneField() {
	$("#rest_phone_error").remove();	
	if($("#rest_phone").val()=='')
	{
		$("#rest_phone").after('<span class="error" id="rest_phone_error"><span>This field is required</span></span>');
		return false;
	}
	else if(IsNumeric($("#rest_phone").val())==false)  
	{
		$("#rest_phone").after('<span class="error" id="rest_phone_error" ><span>Enter valid phone</span></span>');
		return false;	
	}
	else if($("#rest_phone").val().length <10) {
		$("#rest_phone").after('<span class="error" id="rest_phone_error"><span>Enter Valid Phone</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_rest_addressField() {
	$("#rest_address_error").remove();	
	if($("#rest_address").val()=='')
	{
		$("#rest_address").after('<span class="error" id="rest_address_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_rest_mapAddressField(){
	$("#rest_map_address_error").remove();	
	if($("#rest_map_address").val()=='')
	{
		$("#rest_map_address").after('<span class="error" id="rest_map_address_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_rest_deliveryField() {
	$("#rest_delivery_error").remove();	
	if($("#rest_delivery").val()=='')
	{
		$("#rest_delivery").after('<span class="error" id="rest_delivery_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_rest_carryoutField() {
	$("#rest_carryout_error").remove();	
	if($("#rest_carryout").val()=='')
	{
		$("#rest_carryout").after('<span class="error" id="rest_carryout_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function IsNumeric(strString)
{
	var strValidChars = "+-0123456789 (),";
	var strChar;
	var blnResult = true;
	if (strString.length == 0) 
		return false;
	for (i = 0; i < strString.length && blnResult == true; i++)
	{
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1)
		{
			blnResult = false;
		}
	}
	return blnResult;
}	
	
function isDate(dateTimeVal)
{
  $("#"+dateTimeVal+"_error").remove();
  var currVal =  $('#'+dateTimeVal).val();
  //var currVal =  txtDate;
  if(currVal != '') {
	  
	  //Declare Regex  
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	  if (dtArray == null) {
	  	$("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error"><span>Invalid Date</span></span>');
    	 return false;
	  }

	  //Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	
	  if (dtMonth < 1 || dtMonth > 12) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtDay < 1 || dtDay> 31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
		  $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
		  return false;
	  }
	  else if (dtMonth == 2)
	  {
		 var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		 if (dtDay> 29 || (dtDay ==29 && !isleap)) {
			 $("#"+dateTimeVal).after('<span class="error" id="'+dateTimeVal+'_error" ><span>Invalid Date</span></span>');
			  return false;
		 }
	  }
	  $("#"+dateTimeVal+"_error").remove();
 	 return true;
  } else {
	  return true;
  }
}

// ********* To list Restaurants section  ********* //
function restaurants_list(baseurl, currP) {
		var title		= $("#hid_title").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		if(currP) {
			var hidCurrP 	=currP;
		} else {
			var hidCurrP 	= $('#hid_currP').val();
		}
		var limitp=10;
		if(hidCurrP) {
			var startp;
			
			if(hidCurrP==1)
			{
				startp=0;
			}
			else
			{
				startp = (hidCurrP-1)*limitp;
			}
			
		} else {
			var startp=0;
		}
		load_restaurants_list(baseurl,startp,limitp);						  		

		$.ajax({
			type:"POST",
			url:baseurl+"admin/store/restaurant_count",
			data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
			success:function(msg)
			{
				if(msg !=0) {
					if(hidCurrP) { 
						hidCurrP = hidCurrP; 
					} else {
						hidCurrP =1;	
					}
					// Create pagination element
					$("#Pagination").pagination(msg, {
					num_edge_entries: 2,
					num_display_entries: 3,
					callback: pageselectCallbackBanner,
					items_per_page:10,
					current_page:hidCurrP-1
					});
				}
				 else {
					$("#Pagination").css('display','none');
				}
				
			}
				
		});
	/*** pageselectCallback ****/				
	function pageselectCallbackBanner(page_index, jq)
	{
			var page_ind = parseInt(page_index)*parseInt(limitp);
			
			var orderBy 	= $('#hid_orderBy').val();
			var title		= $("#hid_title").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			
			$.ajax({			
				type: "POST",
				url: baseurl+'admin/store/restaurant_list', 
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				success: function(msg){	
				$('#load_restaurants').html('');				 
				$('#load_restaurants').html(msg);			 				
			}				
			});	
	}  
	/*** End pageselectCallback ****/
}

function load_restaurants_list(baseurl,startp,limitp) {
	
	var orderBy 	= $('#hid_orderBy').val();
	var title		= $("#hid_title").val();
	var start_on 	= $('#hid_start_on').val();
	var end_on 	    = $('#hid_end_on').val();
	
	$.ajax({
	type: "POST",
	url: baseurl + "admin/store/restaurant_list/",
	data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
	
	success: function (msg) {
		$('#load_restaurants').html('');
		$('#load_restaurants').html(msg);
	}
	});
}

function sort_restaurants(baseurl,orderBy)
{
	if(orderBy) 
	{	
		orderBy = orderBy;
	}
	else {
		orderBy = '';	
	}

		var startp=0;
	    var limitp=10;
		
		var title		= $("#hid_title").val();
		var start_on 	= $('#hid_start_on').val();
		var end_on 	    = $('#hid_end_on').val();
		
		$('#hid_orderBy').val(orderBy);
		//alert(firstName);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/store/restaurant_list/",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			if(msg)
			{
				$('#load_restaurants').html('');
				$("#load_restaurants").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/store/restaurant_count",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
		success:function(msg)
		{
			if(msg!=0) {
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSort,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	
		/*** pageselectCallback ****/				
		function pageselectCallbackSort(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var title		= $("#hid_title").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/store/restaurant_list', 
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp,
				
				success: function(msg){	
					$('#load_restaurants').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

//Function To Search restaurants
function search_restaurants(baseurl)
{
	if(isDate('start_on') && isDate('end_on'))
    {
		var startp=0;
	    var limitp=10;
		
		var title		= $.trim($("#ser_title").val());
		var start_on 	= $('#start_on').val();
	    var end_on 	    = $('#end_on').val();
		var orderBy 	= $('#hid_orderBy').val();
		
		$('#hid_title').val(title);
		$('#hid_start_on').val(start_on);
	    $('#hid_end_on').val(end_on);
		
		$.ajax({
		type: "POST",
		url: baseurl + "admin/store/restaurant_list",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp,
		success: function(msg){
			
			if(msg)
			{
				
				$('#load_restaurants').html('');
				$("#load_restaurants").html(msg);
			}
		}
		
		});
		$.ajax({
		type:"POST",
		url:baseurl+"admin/store/restaurant_count",
		data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
		
		success:function(msg)
		{
			if(msg!=0) {
				$("#Pagination").css('display','block');
				// Create pagination element
				$("#Pagination").pagination(msg, {
				num_edge_entries: 2,
				num_display_entries: 3,
				callback: pageselectCallbackSearch,
				items_per_page:10
				});	
			}
			else {
				$("#Pagination").css('display','none');
			}
		}
		
		});
	}
		/*** pageselectCallback ****/				
		function pageselectCallbackSearch(page_index, jq)
		{
				var page_ind = parseInt(page_index)*parseInt(limitp);
				
				var title		= $("#hid_title").val();
				var start_on 	= $('#hid_start_on').val();
				var end_on 	    = $('#hid_end_on').val();
				var orderBy 	= $('#hid_orderBy').val();
				
				
				$.ajax({			
				
				type: "POST",
				url: baseurl+'admin/store/restaurant_list',
				data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
				success: function(msg){	
					$('#load_restaurants').html(msg);			 				
				}				
				});	
		}  
		/*** End pageselectCallback ****/
}

function delete_restaurants(baseurl, ctrlfnt) {

	 var current_page = $("[class='current']").html();
	 if(current_page !=null) {
		current_page = current_page; 
	 } else {
		current_page =1; 
	 }
		var startp;
		var limitp=10;
		if(current_page==1)
		{
			startp=0;
		}
		else
		{
			startp = (current_page-1)*limitp;
		}
		
			var title		= $("#hid_title").val();
			var start_on 	= $('#hid_start_on').val();
			var end_on 	    = $('#hid_end_on').val();
			var orderBy 	= $('#hid_orderBy').val();
			
			
		$.ajax({
					type: "POST",
					url: baseurl+ctrlfnt,
					data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+startp+"&limitp="+limitp, 
					success: function(msg){
						//alert(msg);
						if(msg)
						{  
						
							$('#load_restaurants').html('');
							$('#load_restaurants').html(msg);
							$.ajax({
							type:"POST",
							url:baseurl+"admin/store/restaurant_count",
							data:"title="+title+"&start_on="+start_on+"&end_on="+end_on,
							success:function(msg)
							{
								//alert(msg);
								if(msg !=0) { 
									var current_page = $("[class='current']").html();
									// Create pagination element
									$("#Pagination").pagination(msg, {
									num_edge_entries: 2,
									num_display_entries: 3,
									callback: pageselectCallbackBanner,
									items_per_page:10,
									current_page:current_page-1
									
									});
									if((!$('.pagination').find('a').hasClass('current')) && (!$('.pagination').find('span').hasClass('current'))) {
			 								$('.next').prev('a').addClass('current');
											$('.next').prev('a').removeAttr('href');
									}
								} else {
									$("#Pagination").css('display','none');
								}
							}
								
						});
						/*** pageselectCallback ****/				
						function pageselectCallbackBanner(page_index, jq)
						{
								var page_ind = parseInt(page_index)*parseInt(limitp);
								
								var title		= $("#hid_title").val();
								var start_on 	= $('#hid_start_on').val();
								var end_on 	    = $('#hid_end_on').val();
								var orderBy 	= $('#hid_orderBy').val();
								
								
								$.ajax({			
									type: "POST",
									url: baseurl + "admin/store/restaurant_list",
									data:"title="+title+"&start_on="+start_on+"&end_on="+end_on+"&order_by="+orderBy+"&startp="+page_ind+"&limitp="+limitp, 
									success: function(msg){	
									$('#load_restaurants').html('');				 
									$('#load_restaurants').html(msg);		
									 				
								}				
								});	
						}  
						/*** End pageselectCallback ****/
						
						}
					}
					
				});
  
}

function view_restaurants(baseurl, ctrlfnt)	
{
	var current_page = $("[class='current']").html();
	var orderBy 	= $('#hid_orderBy').val();
	$.ajax({			
		type: "POST",
		url: baseurl+ctrlfnt,  
		data: "currP=" + current_page + "&order_by="+orderBy, 
		success: function(msg){	
		
		$('#manage_head').css('display','none');
		$('#search_restaurants').css('display','none');
		$('#Pagination').css('display','none');
			
		$('#load_restaurants').html('');				 
		$('#load_restaurants').html(msg);	
							
	}				
	});	
}

function back_to_restaurants(baseurl, currp, order){
	$('#manage_head').css('display','block');
	$('#search_restaurants').css('display','block');
	$('#Pagination').css('display','block');
	$('#hid_currP').val(currp);
	$('#hid_orderBy').val(order);
	restaurants_list(baseurl,currp);
}