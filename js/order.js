$(document).ready(function(){
	$('.check').click(function () {
		var dishId = $(this).attr('src');
		if($('input[name="sauces_'+dishId+'"]:checked').length>4) {
			$(this).attr('checked', false);
		} 
	});
});

function get_dish_Price(sizeId, dishId){
	$.ajax({		
		type: "POST",
		url: baseurl+"order/get_dish_price", 
		data:"size_id="+sizeId,
		success: function(msg){
			$("#dishPrice"+dishId).html(msg);
		}
	});	
}

function incrementDishCount(dishId){
	var dishVal = $("#dishcount_"+dishId).val();
	if(!isNaN(dishVal))
	dishNewVal = parseInt(dishVal)+parseInt(1);
	else 
	dishNewVal = 1;
	$("#dishcount_"+dishId).val(dishNewVal)
}

function decrementDishCount(dishId){
	var dishVal = $("#dishcount_"+dishId).val();
	if(!isNaN(dishVal) && dishVal!=1)
	dishNewVal = parseInt(dishVal)-parseInt(1);
	
	else 
	dishNewVal = 1;
	
	$("#dishcount_"+dishId).val(dishNewVal)
}

function check_zip(val) {
	if(val==1) {
		$("#order_zip").val('');
		$("#order_zip_error").remove();	
		$('#know_zip').hide();
		$('#dont_know_zip').show();
		$('#order_city').keyup(validate_order_city);
		$('#order_state').change(validate_order_state);
		
	} else {
		$('#order_city').val('');
		$('#order_state').val('');
		$("#order_city_error").remove();
		$("#order_state_error").remove();
		$('#know_zip').show();
		$('#dont_know_zip').hide();
	}
}

function loadState(id) {
	$.ajax({		
		type: "POST",
		url: baseurl+"order/load_states/"+id, 
		data:"",
		success: function(msg){
			$("#state_load").html(msg);
			$("#state_load_unv").html(msg);
			$("#state_load_unv > select").attr("id","order_state_unv");
			$("#state_load_unv > select").attr("name","order_state_unv");
			$("#state_load_unv > select").attr("onChange","loadCampus(this.value);");
			loadCampus('');
		}
	});	
}
function loadStateOnload(id, selState) {
	if(selState) { 
		selState = selState; 
	}
	else { 
		selState =''; 
	}
	$.ajax({		
		type: "POST",
		url: baseurl+"order/load_states/"+id+"/"+selState, 
		data:"",
		success: function(msg){
			$("#state_load").html(msg);
			$("#state_load_unv").html(msg);
			$("#state_load_unv > select").attr("id","order_state_unv");
			$("#state_load_unv > select").attr("name","order_state_unv");
			$("#state_load_unv > select").attr("onChange","loadCampus(this.value);");
		}
	});	
}

function loadAddress(val) {
	if(val=='University') {
		$('#order_address').val('');
		$('#order_floor_type').val('');
		$('#order_floor_no').val('');
		$('#order_zip').val('');
		$('#order_city').val('');
		$('#order_state').val('');
		$("#order_address_error").remove();
		$('#address_normal').hide();
		$('#address_campus').show();
		$('#order_state_unv').change(validate_order_state_unv);
		$('#order_campus').change(validate_order_campus);
		$('#order_campus_dt').change(validate_order_campus_dt);
		$('#order_phone_unv').keyup(validate_order_phone_unv);
	
	} else {
		$('#order_state_unv').val('');
		$('#order_campus').val('');
		$('#order_campus_dt').val('');
		$('#order_room').val('');
		
		$('#address_campus').hide();
		$('#address_normal').show();
	}
}

function loadCampus(id, selCmps) {
	if(selCmps) { 
		selCmps = selCmps; 
	}
	else { 
		selCmps =''; 
	}
	$.ajax({		
		type: "POST",
		url: baseurl+"order/load_campus/"+id+"/"+selCmps, 
		data:"",
		success: function(msg){
			$("#campus_load").html(msg);
			loadCampusDetails(selCmps);
			$('#order_campus').change(validate_order_campus);
		}
	});	
}

function loadCampusOnload(id, selCmps) {
	if(selCmps) { 
		selCmps = selCmps; 
	}
	else { 
		selCmps =''; 
	}
	$.ajax({		
		type: "POST",
		url: baseurl+"order/load_campus/"+id+"/"+selCmps, 
		data:"",
		success: function(msg){
			$("#campus_load").html(msg);
			
		}
	});	
}

function loadCampusDetails(id, selCmpsDt) {
	if(selCmpsDt) { 
		selCmpsDt = selCmpsDt; 
	}
	else { 
		selCmpsDt =''; 
	}
	
	$.ajax({		
		type: "POST",
		url: baseurl+"order/load_campus_details/"+id+"/"+selCmpsDt, 
		data:"",
		success: function(msg){
			$("#campus_dt_load").html(msg);
			$('#order_campus_dt').change(validate_order_campus_dt);
		}
	});	
}

function IsNumeric(strString)
{
   var strValidChars = "0123456789 -";
   var strChar;
   var blnResult = true;
   if (strString.length == 0) return false;

   for (i = 0; i < strString.length && blnResult == true; i++)
	  {
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)
			{
			 blnResult = false;
			}
	  }
   return blnResult;
}

function change_order_address() {
	$('#set_order_address').show();	
}

function switch_order(type) {
	$('#set_order_address').show();	
	$('span.checked').removeClass('checked');
	$(':radio[name="order_type"][value="'+type+'"]').attr('checked', 'checked'); 
	$(':radio[name="order_type"][value="'+type+'"]').parent('span').addClass('checked');
}

function checkValidTest() {
	return true;	
}
function validateAddressForm() {
	var state 	= '';
	var city	= '';
	var checkValid  =  checkValidTest();
	if($('#order_zip').is(':visible')) {
		checkValid  =  checkValid & validate_order_zip();
	}
	if($('#order_address').is(':visible')) {
		checkValid  =  checkValid & validate_order_address();
	}
	if($('#order_floor_no').is(':visible')) {
		checkValid  =  checkValid & validate_order_floor_no();
	}
	
	if($('#order_city').is(':visible')) {
		checkValid  =  checkValid & validate_order_city();
		city		= $('#order_city').val();
	}
	
	if($('#order_state').is(':visible')) {
		checkValid  =  checkValid & validate_order_state();
		state		= $('#order_state').val();
	}
	
	if($('#order_state_unv').is(':visible')) {
		checkValid  =  checkValid & validate_order_state_unv();
		state		= $('#order_state_unv').val();
	}
	
	if($('#order_campus').is(':visible')) {
		checkValid  =  checkValid & validate_order_campus();
	}
	
	if($('#order_campus_dt').is(':visible')) {
		checkValid  =  checkValid & validate_order_campus_dt();
	}
	
	if($('#order_phone').is(':visible')) {
		checkValid  =  checkValid & validate_order_phone();
		var phone	= $('#order_phone').val();
	}
	
	if($('#order_phone_unv').is(':visible')) {
		checkValid  =  checkValid & validate_order_phone_unv();
		var phone	= $('#order_phone_unv').val();
	}
	
	if(checkValid) {
		var type		= $('input:radio[name=order_type]:checked').val();
		var country 	= $('#order_country').val();
		var address_type= $('#order_addtype').val();
		var address		= $('#order_address').val();
		var floor_type	= $('#order_floor_type').val();
		var floor_no	= $('#order_floor_no').val();
		var zip			= $('#order_zip').val();
		
		var campus		= $('#order_campus').val();
		var campus_dt	= $('#order_campus_dt').val();
		var room_no		= $('#order_room').val();
	
		var inform = "type="+type+"&country="+country+"&address_type="+address_type+"&address="+address+"&floor_type="+floor_type+"&floor_no="+floor_no+"&zip="+zip+"&city="+city+"&state="+state+"&campus="+campus+"&campus_dt="+campus_dt+"&room_no="+room_no+"&phone="+phone;
		
		$.ajax({		
			type: "POST",
			url: baseurl+"order/set_address_to_order", 
			data:inform,
			success: function(msg){
				//alert(msg);
				$('#set_order_address').hide();
				$('#location_details').html(msg);
				if(type=='Carryout') {
					$('#load_restaurants').click();
				} else {
					set_delivery_store_order();
				}
				
			}
		});	
	}
}

function set_delivery_store_order() {
	$.ajax({		
		type: "POST",
		url: baseurl+"order/set_delivery_store_address", 
		data:"",
		success: function(msg){
			if(msg) {
				window.location.reload();
			}
		}
	});	
}

function validate_order_address() {
	$("#order_address_error").remove();	
	if($("#order_address").val()=='')
	{
		$("#order_address").after('<span class="error" id="order_address_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_order_zip() {
	$("#order_zip_error").remove();	
	if($("#order_zip").val()=='')
	{
		$("#order_zip").after('<span class="error" id="order_zip_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else if(IsNumeric($("#order_zip").val())==false || $("#order_zip").val().length <5)
	{
	   	$("#order_zip").after('<span class="error" id="order_zip_error" style="left:115px;"><span>Enter Valid Zip</span></span>');
	   	return false;	
	}
	
	else
	{
		return true;
	}
}

function validate_order_phone() {
	$("#order_phone_error").remove();	
	if($("#order_phone").val()=='')
	{
		$("#order_phone").after('<span class="error" id="order_phone_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else if(IsNumeric($("#order_phone").val())==false || $("#order_phone").val().length <10)
	{
	   	$("#order_phone").after('<span class="error" id="order_phone_error" style="left:115px;"><span>Enter Valid Phone</span></span>');
	   	return false;	
	}
	
	else
	{
		return true;
	}
}

function validate_order_phone_unv() {
	$("#order_phone_unv_error").remove();	
	if($("#order_phone_unv").val()=='')
	{
		$("#order_phone_unv").after('<span class="error" id="order_phone_unv_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else if(IsNumeric($("#order_phone_unv").val())==false || $("#order_phone_unv").val().length <10)
	{
	   	$("#order_phone_unv").after('<span class="error" id="order_phone_unv_error" style="left:115px;"><span>Enter Valid Phone</span></span>');
	   	return false;	
	}
	
	else
	{
		return true;
	}
}

function validate_order_city() {
	$("#order_city_error").remove();	
	if($("#order_city").val()=='')
	{
		$("#order_city").after('<span class="error" id="order_city_error" style="left:113px;"><span>This field is required.</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_order_state() {
	$("#order_state_error").remove();	
	if($("#order_state").val()=='')
	{
		$("#order_state").after('<span class="error" id="order_state_error" style="left:219px;"><span>This field is required.</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_order_floor_no() {
	$("#order_floor_no_error").remove();	
	if($('#order_floor_type').val()!='') {
		if($("#order_floor_no").val()=='')
		{
			$("#order_floor_no").after('<span class="error" id="order_floor_no_error" style="left:207px;"><span>This field is required.</span></span>');
			return false;
		}
		else
		{
			return true;
		}
	} else {
		return true;	
	}
}

function validate_order_state_unv() {
	$("#order_state_unv_error").remove();	
	if($("#order_state_unv").val()=='')
	{
		$("#order_state_unv").after('<span class="error" id="order_state_unv_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_order_campus() {
	$("#order_campus_error").remove();	
	if($("#order_campus").val()=='')
	{
		$("#order_campus").after('<span class="error" id="order_campus_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validate_order_campus_dt() {
	$("#order_campus_dt_error").remove();	
	if($("#order_campus_dt").val()=='')
	{
		$("#order_campus_dt").after('<span class="error" id="order_campus_dt_error" style="left:115px;"><span>This field is required.</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}


function set_store_order(sid) {
	$.ajax({		
		type: "POST",
		url: baseurl+"order/set_store_address/"+sid, 
		data:"",
		success: function(msg){
			if(msg) {
				window.parent.location.reload();
				//$("#store_details",window.parent.document).html(msg);
				//close_store_popup();
			}
		}
	});	
}
function close_store_popup() {
	$('#sb-nav-close',window.parent.document).trigger('click');
}

function show_stores() {
	$('#load_restaurants').click();
}

function add_to_cart_single(ds_id) {
	var qty 	= $("#dishcount_"+ds_id).val();
	var cook_time 	= $("#cook_time_"+ds_id).val();
	var sauce		= $("#sauces_"+ds_id).val();
	var sauceSplit	= $("#sauces_split_"+ds_id).val();
	var splitResp 	= sauceSplit.split("|");
	var saucesSplit = splitResp[0];
	var saucePrice  = splitResp[1];
	if(isNaN(qty) || qty <1) { 
		qty = 1; 
	}
	var item_count1 = $("#sel_item_count").val();
	var item_count = parseInt(item_count1) + parseInt(1);
	$.ajax({		
		type: "POST",
		url: baseurl+"order/set_order_dish_single", 
		data:"ds_id="+ds_id+"&quantity="+qty+"&item_id="+item_count+"&sauces="+sauce+"&sauce_split="+saucesSplit+"&saucesplit_price="+saucePrice+"&cook_time="+cook_time,
		success: function(msg){
			if(msg) {
				//$("#sel_item_count").val(item_count);
				window.location.href=baseurl+"order/products#my_cart_right";
			}
		}
	});	
}

function add_item_to_order(ds_id) {	
	$("#my_order_count_span").css('display', 'block');
	$("#total_price_panel").css('display', 'block');
	$("#checkout_button").css('display', 'block');
	var qty			= $("#dishcount_"+ds_id).val();
	var sauce		= $("#sauces_"+ds_id).val();
	var sauceSplit	= $("#sauces_split_"+ds_id).val();
	var splitResp 	= sauceSplit.split("|");
	var saucesSplit = splitResp[0];
	var saucePrice  = splitResp[1];
	if(isNaN(qty) || qty <1) { 
		qty = 1; 
	}
	var price	= $("#dishPrice"+ds_id).html();
	var cook_time	= $("#cook_time_"+ds_id).val();
	var item_count1 = $("#sel_item_count").val();
	var item_count = parseInt(item_count1) + parseInt(1);
	var myOrderCount = $('#my_order_count').html();
	var newMyOrderCount = parseInt(myOrderCount) + parseInt(1);
	
	var subtotPriceVal = $("#subTotalDishPrice").html();
	var newSubTotalPrice = parseFloat(subtotPriceVal) + (parseInt(qty) * parseFloat(price) + parseFloat(saucePrice));
		newSubTotalPrice=newSubTotalPrice.toFixed(2); 
	var vatPercent	= $("#hid_vat_percent").val();	
	var vatPrice 	= (parseFloat(vatPercent) / parseInt(100)) * parseFloat(newSubTotalPrice);
	var	newVatPrice	= vatPrice.toFixed(2); 
	var totalPrice 	= parseFloat(newSubTotalPrice) + parseFloat(vatPrice);
	var	newTotalPrice=totalPrice.toFixed(2); 
	$.ajax({		
		type: "POST",
		url: baseurl+"order/set_order_dish/", 
		data:"ds_id="+ds_id+"&quantity="+qty+"&price="+price+"&item_id="+item_count+"&sauces="+sauce+"&sauce_split="+saucesSplit+"&saucesplit_price="+saucePrice+"&cook_time="+cook_time,
		success: function(msg){
			if(msg) {
				//$('.check').removeAttr('checked');
				$("#add_to_cart_right").append(msg);
				$("#sel_item_count").val(item_count);
				$("#my_order_count").html(newMyOrderCount);
				$("#subTotalDishPrice").html(newSubTotalPrice);
				$("#vat_price").html(newVatPrice);
				$("#totalDishPrice").html(newTotalPrice);
			}
		}
	});	
}

function updateSuaceSplit(sauceSplit, itemId){
	var splitResp 	= sauceSplit.split("|");
	var saucesSplit = splitResp[0];
	var saucePrice  = splitResp[1];
	var oldSaucePrice = $('#saucePrice'+itemId).html();
	var subtotPriceVal = $("#subTotalDishPrice").html();
	var OldsubtotPriceVal = parseFloat(subtotPriceVal) - parseFloat(oldSaucePrice); 
	
	$.ajax({		
		type: "POST",
		url: baseurl+"order/update_order_dish", 
		data:"sauce_split="+saucesSplit+"&sauce_price="+saucePrice+"&item_id="+itemId,
		success: function(msg){
			var newSubTotalPrice = parseFloat(OldsubtotPriceVal) + parseFloat(saucePrice);
			newSubTotalPrice=newSubTotalPrice.toFixed(2);
			var vatPercent	= $("#hid_vat_percent").val();	
			var vatPrice 	= (parseFloat(vatPercent) / parseInt(100)) * parseFloat(newSubTotalPrice);
			var	newVatPrice	= vatPrice.toFixed(2); 
			var totalPrice 	= parseFloat(newSubTotalPrice) + parseFloat(vatPrice);
			var	newTotalPrice=totalPrice.toFixed(2);
			$("#subTotalDishPrice").html(newSubTotalPrice);
			$("#vat_price").html(newVatPrice);
			$("#totalDishPrice").html(newTotalPrice);
			$('#saucePrice'+itemId).html(saucePrice);

		}
	});	
}

function updateSuaceFirst(sauce, itemId){
	$.ajax({		
		type: "POST",
		url: baseurl+"order/update_order_dish", 
		data:"sauce="+sauce+"&item_id="+itemId,
		success: function(msg){
		}
	});	
}


function incrementSelDishCount(itemId){
	var dishVal = $("#seldishcount_"+itemId).val();
	var dishPrice = $("#original_dish_price"+itemId).val();
	//var totPrice	= $(".totalDishPrice").html();
	var subtotPriceVal = $("#subTotalDishPrice").html();
	
	if(!isNaN(dishVal)) { 
		dishNewVal = parseInt(dishVal)+parseInt(1);
	}
	else { 
		dishNewVal = 1;
	}
	
	$.ajax({		
		type: "POST",
		url: baseurl+"order/update_order_dish", 
		data:"quantity="+dishNewVal+"&item_id="+itemId,
		success: function(msg){
			var newDishPrice = parseFloat(dishPrice) * parseInt(dishNewVal);
			$("#seldishcount_"+itemId).val(dishNewVal);
			$("#selDishPrice"+itemId).html(newDishPrice.toFixed(2));
			var newSubTotalPrice = parseFloat(subtotPriceVal) + parseFloat(dishPrice);
			newSubTotalPrice=newSubTotalPrice.toFixed(2);
			var vatPercent	= $("#hid_vat_percent").val();	
			var vatPrice 	= (parseFloat(vatPercent) / parseInt(100)) * parseFloat(newSubTotalPrice);
			var	newVatPrice	= vatPrice.toFixed(2); 
			var totalPrice 	= parseFloat(newSubTotalPrice) + parseFloat(vatPrice);
			var	newTotalPrice=totalPrice.toFixed(2);
			$("#subTotalDishPrice").html(newSubTotalPrice);
			$("#vat_price").html(newVatPrice);
			$("#totalDishPrice").html(newTotalPrice);
			
		}
	});	
}

function decrementSelDishCount(itemId){
	var dishVal = $("#seldishcount_"+itemId).val();
	var dishPrice = $("#original_dish_price"+itemId).val();
	var subtotPriceVal = $("#subTotalDishPrice").html();
	if(!isNaN(dishVal) && dishVal!=1) {
		dishNewVal = parseInt(dishVal)-parseInt(1);
	}
	else {
		dishNewVal = 1;
	}	
	if(!isNaN(dishVal) && dishVal!=1) {
		$.ajax({		
			type: "POST",
			url: baseurl+"order/update_order_dish", 
			data:"quantity="+dishNewVal+"&item_id="+itemId,
			success: function(msg){
				//alert(msg);
				var newDishPrice = parseFloat(dishPrice) * parseInt(dishNewVal);
				$("#seldishcount_"+itemId).val(dishNewVal);
				$("#selDishPrice"+itemId).html(newDishPrice.toFixed(2));
				var newSubTotalPrice = parseFloat(subtotPriceVal) - parseFloat(dishPrice);
				newSubTotalPrice=newSubTotalPrice.toFixed(2);
				var vatPercent	= $("#hid_vat_percent").val();	
				var vatPrice 	= (parseFloat(vatPercent) / parseInt(100)) * parseFloat(newSubTotalPrice);
				var	newVatPrice	= vatPrice.toFixed(2); 
				var totalPrice 	= parseFloat(newSubTotalPrice) + parseFloat(vatPrice);
				var	newTotalPrice=totalPrice.toFixed(2);
				$("#subTotalDishPrice").html(newSubTotalPrice);
				$("#vat_price").html(newVatPrice);
				$("#totalDishPrice").html(newTotalPrice);
			}
		});	
	}
}

function get_selected_dish_Price(sizeId, itemId){
	var dishVal 	= $("#seldishcount_"+itemId).val();
	var dishPrice 	= $("#original_dish_price"+itemId).val();
	var subtotPriceVal = $("#subTotalDishPrice").html();
	var oldTotalPrice = parseFloat(subtotPriceVal) - (parseInt(dishVal) * parseFloat(dishPrice));
	
	$.ajax({		
		type: "POST",
		url: baseurl+"order/get_dish_price", 
		data:"size_id="+sizeId,
		success: function(msg){
			$("#original_dish_price"+itemId).val(msg);
			var newDishPrice =  parseInt(dishVal) * parseFloat(msg);
			$("#selDishPrice"+itemId).html(newDishPrice);
			var newSubTotalPrice = parseFloat(oldTotalPrice) + (parseInt(dishVal) * parseFloat(msg));
			newSubTotalPrice=newSubTotalPrice.toFixed(2);
			var vatPercent	= $("#hid_vat_percent").val();	
			var vatPrice 	= (parseFloat(vatPercent) / parseInt(100)) * parseFloat(newSubTotalPrice);
			var	newVatPrice	= vatPrice.toFixed(2); 
			var totalPrice 	= parseFloat(newSubTotalPrice) + parseFloat(vatPrice);
			var	newTotalPrice=totalPrice.toFixed(2);
			$("#subTotalDishPrice").html(newSubTotalPrice);
			$("#vat_price").html(newVatPrice);
			$("#totalDishPrice").html(newTotalPrice);
			
		}
	});	
	$.ajax({		
		type: "POST",
		url: baseurl+"order/update_order_dish", 
		data:"size_id="+sizeId+"&item_id="+itemId,
		success: function(msg1){
			//alert(msg);
		}
	});	
}

function remove_order_dish(itemId) {
	var itemPrice	= $("#selDishPrice"+itemId).html();
	var saucePrice	= $("#saucePrice"+itemId).html();
	var subtotPriceVal = $("#subTotalDishPrice").html();
	var myOrderCount = $('#my_order_count').html();
	var newMyOrderCount = parseInt(myOrderCount) - parseInt(1);
	
	$.ajax({		
		type: "POST",
		url: baseurl+"order/remove_order_dish", 
		data:"item_id="+itemId,
		success: function(msg){
			var newSubTotalPrice = parseFloat(subtotPriceVal) - parseFloat(itemPrice) - parseFloat(saucePrice);
			newSubTotalPrice=newSubTotalPrice.toFixed(2);
			var vatPercent	= $("#hid_vat_percent").val();	
			var vatPrice 	= (parseFloat(vatPercent) / parseInt(100)) * parseFloat(newSubTotalPrice);
			var	newVatPrice	= vatPrice.toFixed(2); 
			var totalPrice 	= parseFloat(newSubTotalPrice) + parseFloat(vatPrice);
			var	newTotalPrice=totalPrice.toFixed(2);
			$("#subTotalDishPrice").html(newSubTotalPrice);
			$("#vat_price").html(newVatPrice);
			$("#totalDishPrice").html(newTotalPrice);
			$("#my_order_count").html(newMyOrderCount);
			$("#my_cart_single_item"+itemId).remove();
			if(newMyOrderCount==0) {
				$("#my_order_count_span").css('display', 'none');
				$("#total_price_panel").css('display', 'none');
				$("#checkout_button").css('display', 'none');
			}
			//alert(msg);
		}
	});	
}

function show_item_details(itemId) {
	$("#order_item_details"+itemId).css('display', 'block');	
	$("#show_hide_details"+itemId).html("Hide");
	$("#show_hide_details"+itemId).attr('onClick','hide_item_details('+itemId+')');
}

function hide_item_details(itemId) {
	$("#order_item_details"+itemId).css('display', 'none');	
	$("#show_hide_details"+itemId).html("Details");
	$("#show_hide_details"+itemId).attr('onClick','show_item_details('+itemId+')');
}

function proceed_order_checkout() {
	var totalPrice = $("#totalDishPrice").html();
        if(totalPrice)
            window.location.href=baseurl+"order/billing";
//	$.ajax({		
//		type: "POST",
//		url: baseurl+"order/proceed_order_checkout", 
//		data:"total_price="+totalPrice,
//		success: function(msg){
//			window.location.href=baseurl+"order/billing";
//			//alert(msg);
//		}
//	});	
}

function showAddressOrder(catId) {
	window.location.href="#set_order_address";
}

function change_cook_time(item_id,value) {
	$.ajax({		
				type: "POST",
				url: baseurl+"order/update_order_dish", 
				data:"cook_time="+value+"&item_id="+item_id,
				success: function(msg){
				}
	});	
}

// BILLING INFO VALIDATION STARTS 
function getStates(cid) {
	$.ajax({
		type: "POST",
		url: baseurl+"order/load_billing_states/"+cid,  
		data: "", 
		success: function(msg){
                    $('#state_load').html(msg);
                    $("#state").change(validatebillstate);
		}
	});
}

//function for validating First Name
function validateBillFirstName()
{
	$("#user_fname_error").remove();	
	if($.trim($("#firstName").val())=='')
	{
		$("#firstName").after('<span class="error" id="user_fname_error"><span>This field is required</span></span>');
		return false;
	}
	else if(ValidateString($("#firstName").val())==false)
	{
		$("#firstName").after('<span class="error" id="user_fname_error"><span>Characters Only</span></span>');
		return false;	
	}
	else
	{
		return true;
	}
}

//function for validating Last Name
function validateBillLastName()
{
	$("#lastname_error").remove();	
	if($.trim($("#lastName").val())=='')
	{
		$("#lastName").after('<span class="error" id="lastname_error"><span>This field is required</span></span>');
		return false;
	}
	else if(ValidateString($("#lastName").val())==false)
	{
		$("#lastName").after('<span class="error" id="lastname_error"><span>Characters Only</span></span>');
		return false;	
	}
	else
	{
		return true;
	}
}

//function for validating creditCardNumber
function validateBillcreditCardNumber()
{
	$("#creditCardNumber_error").remove();	
	if($.trim($("#creditCardNumber").val())=='')
	{
		$("#creditCardNumber").after('<span class="error" id="creditCardNumber_error"><span>This field is required</span></span>');
		return false;
	}
	else if(isNaN($("#creditCardNumber").val()))
	{
		$("#creditCardNumber").after('<span class="error" id="creditCardNumber_error"><span>Numeric Only</span></span>');
		return false;	
	}
	else
	{
		return true;
	}
}

//function for validating cvvv
function validatecvv2Number()
{
	$("#cvv2Number_error").remove();	
	if($.trim($("#cvv2Number").val())=='')
	{
		$("#cvv2Number").after('<span class="error" id="cvv2Number_error"><span>This field is required</span></span>');
		return false;
	}
	else if(isNaN($("#cvv2Number").val()))
	{
		$("#cvv2Number").after('<span class="error" id="cvv2Number_error"><span>Numeric Only</span></span>');
		return false;	
	}
	else
	{
		return true;
	}
}

function validatebillcountry()
{
	$("#country_error").remove();	
	if($.trim($("#country").val())=='')
	{
		$("#country").after('<span class="error" id="country_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}
function validatebillstate()
{
	$("#state_error").remove();	
	if($.trim($("#state").val())=='')
	{
		$("#state").after('<span class="error" id="state_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validatebillcity()
{
	$("#city_error").remove();	
	if($.trim($("#city").val())=='')
	{
		$("#city").after('<span class="error" id="city_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function validatebilladdress()
{
	$("#address1_error").remove();	
	if($.trim($("#address1").val())=='')
	{
		$("#address1").after('<span class="error" id="address1_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

//function for validating cvvv
function validatebillZip()
{
	$("#zip_error").remove();	
	if($.trim($("#zip").val())=='')
	{
		$("#zip").after('<span class="error" id="zip_error"><span>This field is required</span></span>');
		return false;
	}
	else
	{
		return true;
	}
}

function ValidateString(str){    
	re = /^[A-Za-z ]+$/;
	if(re.test(str))
	{
	   return true;
	}
	else
	{
	   return false;
	}
}

function user_payment_form() {
	
	var payment_type = $('#payment_type').val();
	if(payment_type == '2') {
	$('#billing_details').submit();			
	}
	else  {
	if(validateBillFirstName() & validateBillLastName() & validateBillcreditCardNumber() & validatecvv2Number() & validatebillcountry() & validatebillstate() & validatebillcity() & validatebilladdress() & validatebillZip()) {
	$('#billing_details').submit();		
	}
    }
	
}

function change_to_paypal_pay(val) {
	if(val == '1') {
            $('#bill_li_1').show();
            $('#bill_li_2').show();
            $('#bill_li_3').show();
            $('#bill_li_4').show();
            $('#bill_li_5').show();
            $('#bill_li_6').show();
            $('#bill_li_7').show();
            $('#bill_li_8').show();

            $('#bill_li_9').show();	
            $('#bill_li_10').show();	
            $('#bill_li_11').show();
            $('#bill_li_12').show();
            $('#bill_li_13').show();
	}
	else if(val == '2') {
            $('#bill_li_1').hide();
            $('#bill_li_2').hide();
            $('#bill_li_3').hide();
            $('#bill_li_4').hide();
            $('#bill_li_5').hide();
            $('#bill_li_6').hide();
            $('#bill_li_7').hide();
            $('#bill_li_8').hide();

            $('#bill_li_9').hide();	
            $('#bill_li_10').hide();	
            $('#bill_li_11').hide();
            $('#bill_li_12').hide();
            $('#bill_li_13').hide();
	}
}
