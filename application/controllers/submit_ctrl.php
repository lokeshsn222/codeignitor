

<?php
class submit_ctrl extends CI_Controller {
function __construct() {
parent::__construct();
$this->load->model('submit_model');
}
function index(){
$this->load->library('form_validation'); // Including Validation Library.
$this->form_validation->set_error_delimiters('<div class="error">', '</div>'); // Displaying Errors in Div
$this->form_validation->set_rules('emp_name', 'Username', 'required|min_length[5]|max_length[15]'); // Validation for Name Field
$this->form_validation->set_rules('demail', 'Email', 'required|valid_email'); // Validation for E-mail field.
$this->form_validation->set_rules('dmobile', 'Contact No.', 'required|regex_match[/^[0-9]{10}$/]'); // Validation for Contact Field.
$this->form_validation->set_rules('daddress', 'Address', 'required|min_length[10]|max_length[50]'); // Validation for Address Field.
/*if ($this->form_validation->run() == FALSE) {
echo '121212121';
$this->load->view('welcome_message');
}else {*/
// Initializing database table columns.
$data = array(
'Student_Name' => $this->input->post('emp_name'),
'Student_Email' => $this->input->post('emp_email'),
'Student_Password' => $this->input->post('password'),
'Student_Gender' => $this->input->post('gender'),
'Student_Skills' => $this->input->post('skill_value'),
'Student_Date' => $this->input->post('datepicker'),
);
 
$final_skills = null; 
for($t=0; $t < $data['Student_Skills'];$t++){
  if($t=='0'){
       $final_skills[] = $this->input->post('skill');
    }else{
       $final_skills[] =$this->input->post('skill'.$t);
    }

}
 
$data['skills'] = $final_skills;

$this->submit_model->form_insert($data); // Calling Insert Model and its function.
echo "<script>alert('Form Submitted Successfully....!!!! ');</script>";
$this->load->view('home_page'); // Reloading after submit.
$this->load->helper('url');
    
redirect('list_user');
/*}*/
}




}
?>


