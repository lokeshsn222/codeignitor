<?php
class user extends CI_Model 
{   
    function __construct()
    {       
        parent::__construct();
    $this->load->library('GMaps');
    error_reporting(!E_ALL ^ !E_NOTICE);
    }
  function select() {
    $sql = $this->db->query("select count(gallery_id) as cnt from gallery where status = 1");
    $res = $sql->row()->cnt;
    return $res;  
  }
  
  
  function get_all_photo_gallery($id, $start, $limit) {
    $where ='';
    if($id) 
      $where =" and gallery_id=$id";
    $sql = $this->db->query("select * from gallery where status = 1 ".$where." order by gallery_id desc limit $start, $limit");
    if($sql->num_rows() >0) {
      $res = $sql->result();
      return $res;  
    } else {
      return NULL;  
    } 
  }
  
  function get_all_video_gallery_count() {
    $sql = $this->db->query("select count(video_id) as cnt from videos where status = '1'");
    $res = $sql->row()->cnt;
    return $res;  
  }
  
  
  
  function get_all_video_gallery($id, $start, $limit) {
    $where ='';
    if($id) 
      $where =" and video_id=$id";
    $sql = $this->db->query("select * from videos where status = '1'".$where." order by video_id desc limit $start, $limit");
    if($sql->num_rows() >0) {
      $res = $sql->result();
      foreach($res as $key => $val) {
        $youtubeImg = $val->video_thumb_url;
        $videoPath  = $val->video_path;
        $videoType  = $val->video_type;
        if($videoType=='0') {
          $video_path = str_replace("https://www.youtube.com/watch?v=", "http://www.youtube.com/v/", "$videoPath");
          $res[$key]->video_path =$video_path;  
        }
        
        if($youtubeImg!='') {
          $largeimg = str_replace("default", "0", "$youtubeImg"); 
          $res[$key]->video_thumb_url =$largeimg;
        }
      }
      return $res;  
    } else {
      return NULL;  
    } 
  }
  
  function gallery_pagination($start,$limit,$num_all_count,$page_name) {
    $baseurl = base_url();
    $page='';
    if(!($start > 0)) {                        
      $start = 0;
    } 
    $pagecount  = $start/$limit+1;
    //Previous Code Start Here
    if($start>0)
    {
      $start=$start-$limit;
      $pstart=0;
      $page.='<a class="pagi_prev" href="'.$baseurl.$page_name.'/'.$start.'"> < </a>';
      $start=$start+$limit;
    }
    //Previous Code End Here
    
    $div=$num_all_count/$limit;
    $rem=$num_all_count%$limit;
    if($rem>0)
    {
      $div=$div+1;
    }
    
    if($pagecount<=3)
    {
      $pstart=1;
      $pend=5;
      if($div<$pend)
      {
        $pend=$div;
      }
      $b=0;
    }
    else
    {
      $pstart=$pagecount-2;
      $pend=$pagecount+2;
      if($div<$pend)
      {
        if($div<5) {
          $pstart=1;
          $pend=$div;
        } else {
          $pstart=intval($div-4);
          $pend=$div;
        }
      }
      $b=($pstart-1)*$limit;
    }
    
    for($i=$pstart;$i<=$pend;$i++)
    {
      if($start==$b)
      {
        $page.='<span>'.$i.'</span>';
      }
      else
      {
        $page.=' <a href="'.$baseurl.$page_name.'/'.$b.'"> '.$i.' </a>';
      }
      $b=$b+$limit;
    }
    
    //Next Code Start Here
    $start=$start+$limit;
    if($start<$num_all_count)
    {
      $page.=' <a class="pagi_next" href="'.$baseurl.$page_name.'/'.$start.'"> > </a>';
    }
    $start=$start-$limit;
    //Next Code End Here
    return $page;
  }
  
}