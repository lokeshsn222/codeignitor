-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 26, 2015 at 06:22 PM
-- Server version: 5.5.41
-- PHP Version: 5.5.21-1+deb.sury.org~precise+2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `codeignitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `skill_name`) VALUES
(3, 'php'),
(4, 'cakephp'),
(5, 'html'),
(6, 'java'),
(7, 'codeignitor'),
(8, 'sdsdsdsd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(12) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `date_of_birth`, `sex`) VALUES
(9, 'Lokesh', 'lokeshs@carmatec.com', 'qazplm123', '2014-12-12', 'male'),
(10, 'Vivek', 'Vivek@carmatec.com', 'qazplm123', '0000-00-00', ''),
(11, 'Vivek Raj P', 'Vivek@gmail.com', 'qazplmqwertp', '0000-00-00', ''),
(12, 'Vivek Raj P', 'Vivek@gmail.com', 'qwertyuiop12', '0000-00-00', ''),
(13, 'Vivek Raj P', 'Vivek@gmail.com', '', '0000-00-00', ''),
(14, 'Vivek Raj P', 'Vivek@gmail.com', '', '0000-00-00', ''),
(15, 'Vivek Raj P', 'Vivek@gmail.com', '', '0000-00-00', ''),
(16, 'Vivek Raj P', 'Vivek@gmail.com', '', '0000-00-00', ''),
(17, 'Vivek Raj P', 'Vivek@gmail.com', '', '0000-00-00', ''),
(18, '', '', '', '0000-00-00', ''),
(19, '', '', '', '0000-00-00', ''),
(20, '', '', '', '0000-00-00', ''),
(21, '', '', '', '0000-00-00', ''),
(22, '', '', '', '0000-00-00', ''),
(23, '', '', '', '0000-00-00', ''),
(24, 'Lokesh', 'lokeshs@carmatec.com', 'qazplm123', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

CREATE TABLE IF NOT EXISTS `user_skills` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `skill_id` (`skill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `user_skills`
--

INSERT INTO `user_skills` (`id`, `user_id`, `skill_id`) VALUES
(1, 12, 3),
(2, 12, 6),
(3, 12, 7),
(4, 13, 3),
(5, 13, 6),
(6, 13, 7),
(7, 14, 3),
(8, 14, 6),
(9, 14, 7),
(10, 15, 3),
(11, 15, 6),
(12, 15, 7),
(13, 16, 3),
(14, 16, 6),
(15, 16, 7),
(16, 17, 3),
(17, 17, 6),
(18, 17, 7),
(19, 24, 3),
(20, 24, 8),
(21, 24, 7);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_skills`
--
ALTER TABLE `user_skills`
  ADD CONSTRAINT `user_skills_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_skills_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
