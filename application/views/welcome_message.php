 <?php $this->load->helper('form');?>
<!DOCTYPE html>
<html>
<head>
<title>Sample Registration Form</title>
 
<style type="text/css">


			#container{
				width:960px;
				height:610px;
				margin:50px auto
			}
			#fugo{
				float:right
			}
			form{
				width:320px;
				padding:0 50px 20px;
				background:linear-gradient(#fff,#ABDBFF);
				border:1px solid #ccc;
				box-shadow:0 0 5px;
				font-family:'Marcellus',serif;
				float:left;
				margin-top:10px
			}
			h1{
				text-align:center;
				font-size:28px
			}
			hr{
				border:0;
				border-bottom:1.5px solid #ccc;
				margin-top:-10px;
				margin-bottom:30px
			}
			label{
				font-size:17px
			}
			input{
				width:100%;
				padding:10px;
				margin:6px 0 20px;
				border:none;
				box-shadow:0 0 5px
			}
			input#submit{
				margin-top:20px;
				font-size:18px;
				background:linear-gradient(#22abe9 5%,#36caf0 100%);
				border:1px solid #0F799E;
				color:#fff;
				font-weight:700;
				cursor:pointer;
				text-shadow:0 1px 0 #13506D
			}
			input#submit:hover{
				background:linear-gradient(#36caf0 5%,#22abe9 100%)
			}

			#datepicker{
				width: 12%;
			}

			#submit{
				padding: 10px;
				text-align: center;
				box-shadow: 0 0 5px;
				font-size: 18px;
				background: linear-gradient(#22abe9 5%, #36caf0 100%);
				border: 1px solid #0F799E;
				color: #ffffff;
				font-weight: bold;
				cursor: pointer;
				text-shadow: 0px 1px 0px #13506D;
			 }

			 #submit:hover{
				background: linear-gradient(#36caf0 5%, #22abe9 100%);
			 } 
</style>


  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="js/script.js"></script>
  <script src="js/submit.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
    	//alert('a');
    	$('#add_skills').click(function(){
    			 

    			var i=$("#skillhidden").val();
    			if(!i){
    				$('#add_skills_ha').append('<input type="text" name="skill" ><br>');
    				$("#skillhidden").val(1);
    			}else{
    				
    				$('#add_skills_ha').append('<input type="text" name=skill'+i+' ><br>');
    				$("#skillhidden").val(++i);
    			}
    			
    			


    	});

    });
  </script>

  <script type="text/javascript">
$(document).ready(function(){
$( "#datepicker" ).datepicker();



});
</script>
  

  <link rel="stylesheet" href="runnable.css" />
</head>
<body>
<div id="container">
	<?php echo form_open('submit_ctrl',array('id'=>'main_controller')); ?>
 
<h1>Sample Registration Form</h1>
 
 <?php

$data_email = array(
	'type' => 'name',
	'name' => 'emp_name',
	'id' => 'emp_name',
	'placeholder' => 'Please Enter Name'
	);
echo form_input($data_email);
echo form_error('emp_name');



$data_email = array(
	'type' => 'email',
	'name' => 'emp_email',
	'id' => 'emp_email',
	'placeholder' => 'Please Enter Email'
	);
echo form_input($data_email);




$data_password = array(
	'name' => 'password',
	'id' => 'password',
	'placeholder' => 'Please Enter Password'
);
echo form_password($data_password);

echo form_label('Gender: ', 'gender'); 
echo form_label('Male', 'male') . 
     form_radio(array("name"=>"gender","id"=>"male","value"=>"M", 'checked'=>set_radio('gender', 'M', FALSE))); 
echo form_label('Female', 'female') . 
     form_radio(array("name"=>"gender","id"=>"female","value"=>"F", 'checked'=>set_radio('gender', 'F', FALSE)));
echo form_error('gender'); 
 
 
 

?> 

<div id="add_skills">
     Add Skills
     <input type="hidden" name="skill_value" id="skillhidden" value="">
</div>

<div id="add_skills_ha">
   
</div>


<?php
$data_upload = array(
	'type' => 'file',
	'name' => 'file_upload'
);
echo form_upload($data_upload);
?>

 
<label>Pick a Check Date: </label> <?php
                    $data = array(
                      'name'=> 'datepicker',
                      'id' => 'datepicker',
                      'placeholder' => 'date',
                    );
echo form_input($data);?><br/><br/>

<?php echo form_reset('reset', 'Reset', "class='submit'"); ?>
<?php echo ('<p id="submit">Submit</p>'); ?>
<?php echo form_close(); ?>

</div>
</body>
</html>


 